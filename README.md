Group 7
Samrut Gadde, Hunter Ross, Rhea Shah, Pramit De, Vaishu Vasudevan

**Topic:** Substance Abuse Victims

**Website:** Substance-Abuse.me

**Estimated time to completion:** 15 hours

**Actual time to completion:**
- Samrut: 16 hours
- Hunter: 12 hours
- Rhea: 11 hours
- Pramit: 11 hours
- Vaishu: 13 hours

Git SHA: 47166bb06dcbe3fb9a63271cd91f7d25b5165c83

Pipeline URL: https://gitlab.com/samrut.gadde/cs373-group-07/-/pipelines

**Documentation for API**

https://documenter.getpostman.com/view/12307925/2sA2r6WPkf

**Backend API Link**

https://api.substance-abuse.me/

**PechaKucha Link**

https://youtu.be/aHY7QTYs5qc

**Phase 4: Leader**

Hunter Ross

Phase 4: Leader Responsibilities
- organize team meetings
- go over issues and delegate them to team members
- create a plan to incorporate user stories 
- split responsibility equally for pages and models and instances, along w frontend vs backend


**Purpose:**

Helping raise awareness for various types of substance abuse and where people can go to get treated, as well as how they can get treated

**Model 1: Substances: 150 instances**

Attributes:
- Name
- Stimulant vs depressant
- Products
- Toxcitiy
- Autonomic agents


Media:
- Images
- informational feeds


How instances will link:
- What substances a treatment center focuses on
- What cities see a lot of this drug


**Model 2: Treatment Centers: 2000 instances**

Attributes:
- City
- Size
- State or private
- Minors vs adults
- Type of treatment they focus on

Media:
- Location on a map
- Video relating about the center


How instances will link:
- What drugs this treatment center specializes in treating/types of treatment
- What city it’s in



**Model 3: Cities/Regions with High Drug Use:  50 cities**

Attributes:
- Name
- Marijuana use
- Heroin use
- Cocaine use
- Methamphetamine use


Media:
- Map
- Pictures/slideshow

How instances will link:

- Treatment centers will link if they are contained in that region/city. 
- Substance instances will link if they are commonly abused in that region/city.


**Questions to answer:**
- What are some common substances people may get addicted to and what are the symptoms?
- Where can people go to get help, and how will they be helped?
- What substances are commonly abused in the reader’s city?


**URLs of Data Sources:**
URLs of Data Sources: 
- 	DrugBank API: https://docs.drugbank.com/v1/#get-a-specific-label
-   Drugs, Side Effects and Medical Condition Data Set: https://www.kaggle.com/datasets/jithinanievarghese/drugs-side-effects-and-medical-condition
- 	Finding Treatment Centers: https://findtreatment.gov/locator
- 	Drug Use by City: https://americanaddictioncenters.org/blog/substance-abuse-by-city
- 	City Data: https://www.city-data.com/
-   Google Image API: https://console.cloud.google.com/marketplace/product/google/customsearch.googleapis.com?project=cs373-substance--1707717370991
-   Youtube Data API: https://developers.google.com/youtube/v3



