import csv
import psycopg2
import logging
import sys
import numpy as np
import os
from dotenv import load_dotenv, find_dotenv

load_dotenv(find_dotenv())

logging.basicConfig(level=logging.INFO)

# Connect to the database
conn = psycopg2.connect(
    host=os.getenv("DB_HOST"),
    database=os.getenv("DB_NAME"),
    user=os.getenv("DB_USER"),
    password=os.getenv("DB_PASS")
)

# Open a cursor to perform database operations
cur = conn.cursor()

# for every center in db, update category
cur.execute("SELECT * FROM centers")
centers = cur.fetchall()

for center in centers:
    choice = ["Stimulant", "Depressant"]
    category = np.random.choice(choice, 1)
    cur.execute("UPDATE centers SET category = %s WHERE id = %s", (category[0], center[0]))
    print(f"Center {center[0]} updated to category {category[0]}")

# Make the changes to the database persistent
conn.commit()

# Close communication with the database
cur.close()
conn.close()

logging.info("Centers updated")