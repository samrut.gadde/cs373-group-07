dev:
	npm run dev

build:
	npm run build

server:
	npm run server

commitpush:
	git add .
	git commit -m "changes made"
	git push origin main

pythonreqs:
	pip install -r backendrequirements.txt