import xml.etree.ElementTree as ET
import csv
import re


def __main__():
    #goal of this file is to parse substances.xml data and insert it into a CSV file.
    #we will then insert the data in the CSV file into the database.

    #debug variable used for observing a few values and keeping python window open
    debug = 0

    #this step takes a while because xml file is 37 million lines long
    print("pre parse, should take a while")
    tree = ET.parse('public/substances.xml') #need to download this file from drugbank website & rename to substances

    print("post parse")
    root = tree.getroot()
    #print(root)

    ns = {'prefix':'http://www.drugbank.ca'}
    count = 0 #num of substances recorded

    data = []
    #insert data from xml file into data array. data array will be a list of lists of strings
    try:
        for child in root:
            groups = child.find('prefix:groups', ns)
            for group in groups:
                if group.text == "illicit":
                    desc = child.find('prefix:description', ns).text
                    #only want substances with a description
                    if desc is not None:
                        name = child.find('prefix:name', ns).text
                        toxicity = child.find('prefix:toxicity', ns).text
                        indication = child.find('prefix:indication', ns).text
                        #basic category
                        category = 'Other'
                        if 'stimulant' in desc:
                            category = 'Stimulant'
                        elif 'depressant' in desc:
                            category = 'Depressant'
                        #categories list
                        categories = []
                        cats = child.find('prefix:categories', ns)
                        if cats is not None:
                            cats = cats.findall('prefix:category', ns)
                            for cat in cats:
                                catName = cat.find('prefix:category', ns).text
                                #if no category is found we check within the category name pulled from xml
                                if category == 'Other':
                                    if 'Stimulant' in catName:
                                        category = 'Stimulant'
                                    elif 'Depressant' in catName:
                                        category = 'Depressant'
                                if catName not in categories:
                                    categories.append(catName)
                        #add products and & international brands
                        productList = []
                        products = child.find('prefix:products', ns).findall('prefix:product', ns)
                        if products:
                            for product in products:
                                productName = product.find('prefix:name', ns).text
                                if productName not in productList:
                                    productList.append(productName)
                        products = child.find('prefix:international-brands', ns).findall('prefix:international-brand', ns)
                        for product in products:
                            productName = product.find('prefix:name', ns).text
                            if productName not in productList:
                                productList.append(productName)
                        #need them to be empty strings, not None
                        if toxicity is None:
                            toxicity = ""
                        if indication is None:
                            indication = ""
                        # print(name, "\n", desc)
                        # print(type(name), type(desc))
                        data.append([name, desc, indication, toxicity, ','.join(str(prod) for prod in productList), ','.join(str(cat) for cat in categories), category])
                        count += 1
    except Exception as e:
        print(e)
    #print(data)
    print(count)
    processed_data = []
    #want to process data to remove weird formatting information bc it messes up csv file
    try:
        for row in data:
            processed_row = [re.sub(r'\[.*?\]', '', item) for item in row] #removes [] and info inside them
            processed_row = [re.sub(r'[\r\n]', '', item) for item in processed_row] #removes \r & \n
            processed_data.append(processed_row) 
        #print(processed_data)
    except Exception as e:
        print(e)

    #insert the data from processed_data to the CSV file
    try:
        csv_file_path = 'public/substancesCSV.csv'
        with open(csv_file_path, 'w', encoding = 'utf-8', newline='') as file:
            writer = csv.writer(file, quoting=csv.QUOTE_NONNUMERIC)
            writer.writerow(['Name','Description', 'Indication', 'Toxicity', 'Products', 'Categories', 'Category'])
            writer.writerows(processed_data)
        print("Data write complete")
    except Exception as e:
        print(e)

    #keeps window open
    while(debug):
        pass

    return count

if __name__ == '__main__':
    __main__()
        