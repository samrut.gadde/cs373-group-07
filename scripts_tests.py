import unittest
import io
import sys
from unittest.mock import patch, MagicMock
"""
Originally, this file had to be in the same directory as the scripts it was testing,
however, I believe this can be fixed as I believe the issue lied in the actual script themselves.
TO DO: move to tests folder. have to fix import path for the scripts when you do so.
"""
import insertSubstances
import insertCenters
import parse_substances

class Tests(unittest.TestCase):
    @patch('parse_substances.__main__')
    def test_parse(self, mock_function):
        mock_function.return_value = 145
        result = parse_substances.__main__()
        self.assertEqual(result, 145)

    #Next two tests only test if we called execute, could update for actual calls but I didn't want to accidentally mess anything up
    @patch('insertSubstances.psycopg2.connect')
    def test_insertSubstances(self, mock_connect):
        mock_cursor =  mock_connect.return_value.cursor.return_value
        insertSubstances.__main__()
        mock_cursor.execute.assert_called()
    
    @patch('insertCenters.psycopg2.connect')
    def test_insertCenters(self, mock_connect):
        mock_cursor =  mock_connect.return_value.cursor.return_value
        insertCenters.__main__()
        mock_cursor.execute.assert_called()
        
if __name__ == '__main__':
    unittest.main()
