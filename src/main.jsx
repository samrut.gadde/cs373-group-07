import React from 'react';
import ReactDOM from 'react-dom';
import {
  createBrowserRouter,
  RouterProvider
} from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import Splash from './pages/Splash/Splash.jsx';
import About from './pages/About/About.jsx';
import Substances from './pages/Substances/Substances.jsx';
import TreatmentCenters from './pages/TreatmentCenters/TreatmentCenters.jsx';
import NavbarComponent from './components/Navbar/Navbar.jsx';
import CenterSlug from './pages/CenterSlug/CenterSlug.jsx';
import Cities from './pages/Cities/Cities.jsx';
import './index.css';
import './App.css';
import CitySlug from './pages/CitySlug/CitySlug.jsx';
import SubstanceSlug from './pages/SubstanceSlug/SubstanceSlug.jsx';
import Search from './pages/Search/Search.jsx';
import Visualizations from './pages/Visualizations/Visualizations.jsx';
import DeveloperVisualizations from './pages/DeveloperVisualizations/DeveloperVisualizations.jsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <Splash />,
  },
  {
    path: '/about',
    element: <About />,
  },
  {
    path: '/substances',
    element: <Substances />,
  },
  {
    path: '/substances/:slug',
    element: <SubstanceSlug/>
  },
  {
    path: '/cities',
    element: <Cities />,
  },
  {
    path: '/centers',
    element: <TreatmentCenters />,
  },
  {
    path: '/centers/:slug',
    element: <CenterSlug />,
  },
  {
    path: '/cities/:slug',
    element: <CitySlug/>
  },
  {
    path: '/search',
    element: <Search/>
  },
  {
    path: '/visualizations',
    element: <Visualizations/>
  },
  {
    path: '/dev-visualizations',
    element: <DeveloperVisualizations/>
  }
]);

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <div className="app-container">
      <NavbarComponent className="navbar"/>
      <RouterProvider router={router} />
    </div>
  </React.StrictMode>
);
