

import React from "react";
import { useLocation } from "react-router-dom";
import axios from "axios";
import Papa from "papaparse";
import Card from "react-bootstrap/Card";
import Row from "react-bootstrap/Row";
import { Link } from "react-router-dom";
import slugify from 'react-slugify'
import apiLink from "../../api.config";
import Button from "react-bootstrap/Button";
import "./CenterSlug.css";

// Get related substances from random page 
async function fetchSubstances(category) {
  const pageSize = 3;

  // First get total number of substances
  let totalSubstances = 0;
  await axios.get(apiLink + `/substances/count?category=${category}`).then((response) => {
    totalSubstances = response.data.count;
  })

  // Then get a random page
  const randomPage = Math.floor(Math.random() * (totalSubstances / pageSize));
  let substances = [];
  await axios.get(apiLink + `/substances?pageSize=${pageSize}&page=${randomPage}&category=${category}`).then((response) => {
    substances = response.data;
  })

  return substances;
}

// Get linked city for this center
async function fetchCity(cityName) {
  let city = {};
  await axios.get(apiLink + `/cities?name=${cityName}`).then((response) => {
    city = response.data[0];
  })

  return city;
}

// Get the services associated with this center
export async function fetchServices() {
  let services = {};
  await axios.get("/code_reference.csv").then((response) => {
    const results = Papa.parse(response.data, {
      header: true,
    });
    services = results.data;
  })

  return services;
}

// display fetched services on the page by organizing them by category
function DisplayServices({ services }) {
  const allCategories = services.map((service) => {
    return service["category_name"];
  });
  const categories = allCategories.filter((category, index) => {
    return allCategories.indexOf(category) === index;
  });

  return (
    <div className="service-container">
      {categories.map((category, index) => {
        return (
          <div key={index} className="tc-card">
            <h5 className="tc-card-header">{category}</h5>
            <div className="tc-list-container">
              <ul>
                {services.map((service, index) => {
                  if (service["category_name"] === category) {
                    return <li key={index}>{service["service_name"]}</li>;
                  }
                })}
              </ul>
            </div>
          </div>
        );
      })}
    </div>
  );
}

export default function CenterSlug(props) {
  const { state } = useLocation();
  const { center } = state;
  const [services, setServices] = React.useState([]);
  const [mappedServices, setMappedServices] = React.useState([]);
  const [videoId, setVideoId] = React.useState([]);
  const [city, setCity] = React.useState({});
  const [substances, setSubstances] = React.useState([]);
  const API_KEY = 'AIzaSyA9W82TLurnDqqGbFNwtOO5JB_qlxv7VZ4';
  const keyword = center.name1;
  const maxResults = 1;
  const [page, setPage] = React.useState(1);
  const totalSubstancesCount = 3;
  const pageSize = 20;

  let mapUrl = `https://maps.google.com/maps?q=${center.latitude},${center.longitude}&z=15&output=embed`;

  // gets and sorts the services, then the linked city/substances
  React.useEffect(() => {
    fetchServices().then((services) => {
      setServices([]);
      setMappedServices([]);
      setServices(services);

      services.forEach((service) => {
        if (center[service["service_code"]?.toLowerCase()]) {
          setMappedServices((mappedServices) => [...mappedServices, service]);
        }
      });
    });

    fetchCity(center.city).then((city) => {
      setCity(city);
    });

    fetchSubstances(center.category).then((substances) => {
      setSubstances(substances);
    });

  }, []);

  // gets the relevant YouTube video for this center
  React.useEffect(() => {
    fetch(`https://www.googleapis.com/youtube/v3/search?key=${API_KEY}&q=${keyword}&part=snippet&type=video&maxResults=${maxResults}`)
      .then(response => response.json())
      .then(data => {
        const firstVideoId = data.items[0].id.videoId;
        setVideoId(firstVideoId);
      })
  }, []);

  return (
    <div className="center-slug-container">
      <div className="top-container">
        <div className="left-container">
          <div className="header-container">
            <div className="name-container">
              <h1>{center.name1}</h1>
              <h2>{center.name2}</h2>
            </div>
            <div className="address-container">
              <h4>{center.city}, {center.state} {center.zip}</h4>
              <h5 style={{ textDecoration: 'none', color: '#808080' }}>{center.street1}</h5>
              <h5 style={{ textDecoration: 'none', color: '#808080' }}>{center.street2}</h5>
              <h5 style={{ textDecoration: 'none', color: '#808080' }}>{center.phone}</h5>
              <button className="hyperlink-button" onClick={() => window.location.href = center.website}>
                Go to Website
              </button>
            </div>
            <div className="city-card-container">
              <div>
                <h4>City Information</h4>
              </div>
              <Card className="city-card">
                <Link to={`/cities/austin-tx`} state={{ city: city }} className='link'>
                  <Card.Body>
                    <Card.Title>{center.city}</Card.Title>
                    <Card.Text>
                      <p>
                        <strong>Marijuana Use Rate:</strong> {city.marijuana} %
                      </p>
                      <p>
                        <strong>Cocaine Use Rate:</strong> {city.cocaine} %
                      </p>
                      <p>
                        <strong>Heroin Use Rate:</strong> {city.heroin} %
                      </p>
                      <p>
                        <strong>Meth  Use Rate:</strong> {city.meth} %
                      </p>
                      <p>
                        <strong>Region:</strong> {city.region}
                      </p>
                    </Card.Text>
                  </Card.Body>
                </Link>
              </Card>
            </div>
          </div>
          <div className="media-container">
            <div className="video-container">
              <iframe
                src={`https://www.youtube.com/embed/${videoId}`}
                title="YouTube video player"
                frameborder="0"
                allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share"
                allowfullscreen

              ></iframe>
            </div>
            <div className="map-container">
              <iframe src={mapUrl} />
            </div>
          </div>
        </div>
        <div className="right-container">
          <DisplayServices services={mappedServices} />
        </div>
      </div>
      <div className="bottom-container">
        <div className="related-title">
          <h3><b>Related Drugs</b></h3>
          <h5><b>This center treats addiction relating to these drugs: </b></h5>
        </div>
        <div className="bottom-rows">
          <div className="pagination-info-centers">
            <p>
              Displaying results {+((page - 1) * pageSize) + 1} -{" "}
              {+(Math.min((page) * pageSize - 1, totalSubstancesCount))} out
              of {+(totalSubstancesCount)} total instances
            </p>
            <p>
              Page {page} of results.
            </p>
            <div className="next-button-container"  >
              <div className="top-buttons" >
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page - 1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  Previous
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page + 1)}
                  disabled={page * pageSize >= totalSubstancesCount}
                  className="substance-pagination"
                >
                  Next
                </Button>
              </div>
              <div className="buttom-buttons" style={{ marginLeft: '1.5rem' }}>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  First
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(Math.ceil(totalSubstancesCount / pageSize))}
                  disabled={page * pageSize >= totalSubstancesCount}
                  className="substance-pagination"
                >
                  Last
                </Button>
              </div>
            </div>
          </div>
          <div className="bottom-content">
            <Row className="justify-content-center">
              {Array.isArray(substances) && substances?.map((substance, index) => {
                return (
                  <Card key={index} className="card">
                    <Link
                      to={`/substances/${slugify(substance.name)}`}
                      state={{ substance: substance }}
                      className="link"
                    >
                      <Card.Body>
                        <Card.Title>{substance.name}</Card.Title>
                        <Card.Subtitle>{substance.category}</Card.Subtitle>
                        <Card.Text>
                          <br />
                          {substance.description.substring(0, 200) + "..."}
                        </Card.Text>
                      </Card.Body>
                    </Link>
                  </Card>
                );
              })}
            </Row>
          </div>
        </div>
      </div>
    </div>
  );
}
