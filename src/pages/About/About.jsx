import React from "react";
import AboutCardComponent from "./components/About-Card.jsx";
import ToolCardComponent from "./components/Tool-Card.jsx";
import LinkedButtonComponent from "./components/Linked-Button.jsx";
import axios from "axios";
import "./About.css";

export default function About() {
  const [commits, setCommits] = React.useState(0);
  const [issues, setIssues] = React.useState(0);
  const [commitMap, setCommitMap] = React.useState({});
  const [issueMap, setIssueMap] = React.useState({});

  // Gets info from GitLab API
  async function fetchGitData() {
    let commits = [];
    let issues = [];

    let commitMap = {
      SamrutGadde: 0,
      "Hunter Ross": 0,
      rheajshah: 0,
      "Pramit De": 0,
      "Vaishu Vasudevan": 0,
    };

    let issueMap = {
      "samrut.gadde": 0,
      hunteross: 0,
      rheajshah1: 0,
      pramitde: 0,
      "vaishu-vasudevan": 0,
    };

    setIssues(0);
    setCommits(0);

    let page = 1;
    let per_page = 100;
    let done = false;
    while (true) {
      await axios
        .get(
          `https://gitlab.com/api/v4/projects/54574798/repository/commits?page=${page++}&per_page=${per_page}`
        )
        .then((response) => {
          if (response.data.length == 0) {
            done = true;
          }

          commits = commits.concat(response.data);
        })

      if (done) {
        break;
      }
    }

    setCommits(commits.length);

    for (let commit of commits) {
      let author = commit.author_name;
      if (author in commitMap) {
        commitMap[author]++;
      }
    }

    setCommitMap(commitMap);

    page = 1;
    done = false;

    while (true) {
      await axios
        .get(
          `https://gitlab.com/api/v4/projects/54574798/issues?page=${page++}&per_page=${per_page}&state=closed`
        )
        .then((response) => {
          if (response.data.length == 0) {
            done = true;
          }

          issues = issues.concat(response.data);
        })
        .catch((error) => {
          // console.log(error);
        });

      if (done) {
        break;
      }
    }

    setIssues(issues.length);

    for (let issue of issues) {
      let closedBy = issue.closed_by.username;
      if (closedBy in issueMap) {
        issueMap[closedBy]++;
      }
    }
    setIssueMap(issueMap);
  }

  React.useEffect(() => {
    fetchGitData();
  }, []);

  return (
    <div className="about-container">
      <div className="text-center about-page-section">
        <h1 className="about-title-style">
          About SAA (Substance Abuse Awareness)
        </h1>
        <p>
          Substance Abuse Awareness serves as a comprehensive information
          center, offering insights into common drugs, treatment centers, and
          statistics about popular cities where substance abuse is prevalent.
          Our mission is to empower those struggling with addiction nationwide
          by providing accessible resources, informing about the consequences of
          taking certain drugs, and facilitating change by pointing you in the
          direction of various rehab centers.
        </p>
      </div>

      <div className="team-container">
        <h2 className="text-center about-title-style">Meet the Team</h2>
        <div className="card-container d-flex justify-content-center">
          <AboutCardComponent
            member={"Samrut Gadde"}
            git_user={"@samrut.gadde"}
            img_name={"/samrut-pfp.jpg"}
            bio={
              "Samrut is a sophomore CS major. He is from Katy, Texas, and enjoys playing the guitar in his free time."
            }
            commits={commitMap["SamrutGadde"]}
            issues={issueMap["samrut.gadde"]}
            roles={"backend"}
            tests={"11"}
          />

          <AboutCardComponent
            member={"Hunter Ross"}
            git_user={"@hunteross"}
            img_name={"/hunter-pfp.JPG"}
            bio={
              "Hunter is a junior CS major with a minor in business. In his free time he enjoys cooking, gaming, and playing sports with friends."
            }
            commits={commitMap["Hunter Ross"]}
            issues={issueMap["hunteross"]}
            roles={"backend"}
            tests={"11"}
          />

          <AboutCardComponent
            member={"Rhea Shah"}
            git_user={"@rheajshah1"}
            img_name={"/rhea-pfp.JPG"}
            bio={
              "Rhea is a 2nd year CS major with a minor in entrepreneurship. She is also a dancer on Texas Mohini and loves watching The Office!"
            }
            commits={commitMap["rheajshah"]}
            issues={issueMap["rheajshah1"]}
            roles={"frontend"}
            tests={"12"}
          />

          <AboutCardComponent
            member={"Pramit De"}
            git_user={"@pramitde"}
            img_name={"/pramit-pfp.jpeg"}
            bio={
              "Pramit is a junior majoring in Computer Science and minoring in Business. He is from Houston, TX and enjoys going to the gym in his free time."
            }
            commits={commitMap["Pramit De"]}
            issues={issueMap["pramitde"]}
            roles={"backend"}
            tests={"11"}
          />

          <AboutCardComponent
            member={"Vaishu Vasudevan"}
            git_user={"@vaishu-vasudevan"}
            img_name={"/vaishu-pfp.jpeg"}
            bio={
              "Vaishu is a junior and a computer science major at UT, and in her free time, she likes to bake and play the piano."
            }
            commits={commitMap["Vaishu Vasudevan"]}
            issues={issueMap["vaishu-vasudevan"]}
            roles={"frontend"}
            tests={"12"}
          />
        </div>

        <div className="text-center">
          <p>
            <b>Total Commits:</b> {commits}
          </p>
          <p>
            <b>Total Closed Issues:</b> {issues}
          </p>
        </div>
      </div>

      <section className="about-page-section">
        <h2 className="section-header">Tools Used</h2>
        <div className="tool-container">
          <ToolCardComponent
            img_name={"/aws-tool.png"}
            tool={"AWS"}
            desc={"Used to host website, aws rds, aws amplify, aws api gateway"}
          />

          <ToolCardComponent
            img_name={"/bootstrap-tool.png"}
            tool={"Bootstrap"}
            desc={"Used to style the frontend and make design responsive."}
          />

          <ToolCardComponent
            img_name={"/flask-tool.png"}
            tool={"Flask"}
            desc={"Micro Web Framework to help build our API"}
          />

          <ToolCardComponent
            img_name={"/postgresql-tool.png"}
            tool={"PostgreSQL"}
            desc={"Object-relational database system"}
          />

          <ToolCardComponent
            img_name={"/postman-tool.svg"}
            tool={"Postman"}
            desc={"Used to develop our API"}
          />

          <ToolCardComponent
            img_name={"/react-tool.png"}
            tool={"React"}
            desc={"Used to code the frontend."}
          />

          <ToolCardComponent
            img_name={"/d3-tool.png"}
            tool={"D3"}
            desc={"Used to create visualizations."}
          />
        </div>
      </section>

      <section className="about-page-section">
        <h2 className="section-header">
          Interesting Results of Integrating Disparate Data Sources
        </h2>
        <p className="abt-page-integrate-txt">
          By integrating many different data sources, including those that focus
          on Substances, Treatment Centers, and Cities, we were able to create a
          comprehensive resource for those struggling with addiction. This allows us to see the bigger picture of substance abuse in the United States and
          provide a resource for those who need help, whether it be in the form
          of information or treatment.
        </p>
      </section>

      <section className="about-page-section">
        <h2 className="section-header">Data Sources</h2>
        <div className="abt-src-container">
          <a href="https://findtreatment.gov" target="_blank">findtreatment.gov</a>
          <p>A dataset for treatment centers in the United States</p>
        </div>
        <div className="abt-src-container">
          <a href="https://docs.drugbank.com/v1/#get-a-specific-label" target="_blank">DrugBank API</a>
          <p>A RESTful API for information on drugs</p>
        </div>
        <div className="abt-src-container">
          <a href="https://www.city-data.com/" target="_blank">City Data</a>
          <p>A data source to view information about cities</p>
        </div>
        <div className="abt-src-container">
          <a href="https://developers.google.com/youtube/v3" target="_blank">Youtube Data API</a>
          <p>An API to embed Youtube videos into applications</p>
        </div>
      </section>

      <section className="about-page-section">
        <LinkedButtonComponent
          name={"Gitlab Repo"}
          link={"https://gitlab.com/samrut.gadde/cs373-group-07"}
        />
        <LinkedButtonComponent
          name={"API Documentation"}
          link={"https://documenter.getpostman.com/view/12307925/2sA2r6WPkf"}
        />
        <LinkedButtonComponent
          name={"PechaKucha"}
          link={"https://youtu.be/aHY7QTYs5qc"}
        />
      </section>
    </div>
  );
}
