import React from 'react'
import './Linked-Button.css'

const LinkedButtonComponent = ({ name, link }) => {
  return (
    <div className="button-card">
      <div className="button-card-header">
        <a className="button-title" href={link} target="_blank">{name}</a>
      </div>
    </div>
  );
};

export default LinkedButtonComponent;