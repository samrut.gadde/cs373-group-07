import React from 'react'
import './About-Card.css'

const AboutCardComponent = ({ member, git_user, img_name, bio, commits, issues, roles, tests }) => {
  return (
    <div className="about-card">
      <div className="abt-card-header">
        <img src={img_name} className="abt-card-img" alt="profile picture" />
        <div className="abt-title-header">
          <h2>{member}</h2>
          <h3>{git_user}</h3>
        </div>
      </div>
      <p className="abt-bio">{bio}</p>
      <div className="git-container">
        <p>Commits: {commits}</p>
        <p>Issues closed: {issues}</p>
        <p>Role(s): {roles}</p>
        <p>Unit Tests: {tests}</p>
      </div>
    </div>
  );
};

export default AboutCardComponent;