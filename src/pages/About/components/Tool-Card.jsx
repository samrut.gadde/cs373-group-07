import React from 'react'
import './Tool-Card.css'

const ToolCardComponent = ({ img_name, tool, desc }) => {
  return (
    <div className="tool-card">
      <div className="tool-card-header">
        <img src={img_name} className="tool-card-img" alt="tool" />
        <div className="tool-title">
          <h2>{tool}</h2>
          <p className="desc">{desc}</p>
        </div>
      </div>
    </div>
  );
};

export default ToolCardComponent;