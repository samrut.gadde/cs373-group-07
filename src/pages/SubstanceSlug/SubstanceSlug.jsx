import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom'
import Card from 'react-bootstrap/Card'
import axios from 'axios';
import { Button } from 'react-bootstrap';
import apiLink from '../../api.config';
import { Link } from 'react-router-dom';
import slugify from 'react-slugify'
import ListGroup from 'react-bootstrap/ListGroup';
import './SubstanceSlug.css'

// Function for fetching the city image
const getDrugPic = async (drugName) => {
  const searchEngineID = "3704f038e1c3a4284";
  const key = "AIzaSyA9W82TLurnDqqGbFNwtOO5JB_qlxv7VZ4";
  const url = `https://www.googleapis.com/customsearch/v1?key=${key}&cx=${searchEngineID}&q=${drugName}&searchType=image`;

  try {
    const response = await fetch(url);
    const data = await response.json();

    if (data && data.items && data.items.length > 0) {
      const imageURL = data.items[0].link;
      return imageURL;
    } else {
      return null;
    }
  } catch (error) {
    return null;
  }
};

// Gets centers that treat this drug addiction
async function fetchCenters(category) {
  const pageSize = 3;
  // First get total number of centers
  let totalCenters = 0;
  await axios
    .get(apiLink + `/centers/count?category=${category}`)
    .then((response) => {
      totalCenters = response.data.count;
    });

  // Then get a random page
  const randomPage = Math.floor(Math.random() * (totalCenters / pageSize));
  let centers = [];
  await axios.get(apiLink + `/centers?page=${randomPage}&pageSize=${pageSize}&category=${category}`).then((response) => {
    centers = response.data;
  })

  return centers;
}

// Get the cities that see high rates of abuse for this drug
async function fetchCities(category) {
  const pageSize = 3;
  // First get total number of cities
  let totalCities = 0;
  await axios.get(apiLink + `/cities/count?leadingcategory=${category}`).then((response) => {
    totalCities = response.data.count;
  })

  // Then get a random page
  const randomPage = Math.floor(Math.random() * (totalCities / pageSize));
  let cities = [];
  await axios.get(apiLink + `/cities?page=${randomPage}&pageSize=${pageSize}&leadingcategory=${category}`).then((response) => {
    cities = response.data;
  })

  return cities;
}

export default function SubstanceSlug(props) {
  const { state } = useLocation();
  const { substance } = state;
  let products = ["There are no products available"];
  if (substance.products) {
    products = substance.products.split(",");
  }
  let categories = ["There are no categories available"]
  if (substance.categories) {
    categories = substance.categories.split(",");
  }
  const cnsCategory = substance.category;
  const wikipediaLink = "https://en.wikipedia.org/w/index.php?search=" + substance.name + "&title=Special:Search&profile=advanced&fulltext=1&ns0=1"
  const [drugImage, setDrugImage] = useState(null);
  const [centers, setCenters] = useState([]);
  const [cities, setCities] = useState([]);
  const [page, setPage] = React.useState(1);
  const totalCityCount = 3;
  const totalCenterCount = 3;
  const pageSize = 20;

  // Gets the image for the drug
  useEffect(() => {
    const fetchDrugImage = async () => {
      const imageURL = await getDrugPic(substance.name);
      setDrugImage(imageURL);
    };

    fetchDrugImage();
  }, [substance.name]);

  // Gets the linked cities and centers for this substance
  useEffect(() => {
    async function fetchData() {
      let category = substance.category === "Other" ? "Stimulant" : substance.category;
      await fetchCenters(category).then((centers) => {
        setCenters(centers);
      });

      await fetchCities(category).then((cities) => {
        setCities(cities);
      });
    }

    fetchData();
  }, []);

  // updates the cities on page update
  useEffect(() => {
    fetchCities(pageSize, page).then((cities) => {
      setCities(cities);
    });
  }, [page]);

  return (
    <div className="sub-slug-container">
      <div className="sub-slug-header">
        <div className="sub-title-text">
          <h1 className="sub-title">{substance.name}</h1>
          <h2 className="sub-category" style={{ textDecoration: 'none', color: '#808080' }}>{cnsCategory}</h2>
        </div>
        <div className="sub-img-container">
          <img className="sub-drug-img" src={drugImage} />
        </div>
      </div>
      <div className="sub-info-body">
        <div className="sub-text-info">
          <div className="sub-info">
            <h1 className="sub-info-title">Description</h1>
            <p className="sub-info-content">{substance.description}</p>
          </div>
          <div className="sub-info">
            <h1 className="sub-info-title">Indication</h1>
            <p className="sub-info-content">{substance.indication}</p>
          </div>
          <div className="sub-info">
            <h1 className="sub-info-title">Toxicity</h1>
            <p className="sub-info-content">{substance.toxicity}</p>
          </div>
        </div>
        <div className="sub-list-info">
          <div className="sub-info">
            <h1 className="sub-info-title">Products</h1>
            <ul className="sub-info-content">
              {products?.map((product, index) => (
                <li key={index}>{product}</li>
              ))}
            </ul>
          </div>
          <div className="sub-info">
            <h1 className="sub-info-title">Categories</h1>
            <ul className="sub-info-content">
              {categories?.map((category, index) => (
                <li key={index}>{category}</li>
              ))}
            </ul>
          </div>
        </div>
      </div>
      <div className="media-container">
        <div className="sub-wiki-container">
          <iframe
            src={wikipediaLink}
            title="Wikipedia"
            className="sub-wiki"
          ></iframe>
        </div>
      </div>
      <div className="sub-related-cities">
        <h1 className="sub-related-title">Related Cities</h1>
        <h5 class="sub-header"><b>These cities see high rates of addiction of this drug: </b></h5>
        <div className="sub-cities-rows">
          <div className="pagination-info">
            <p>
              Displaying results {+(page - 1) * pageSize + 1} -{" "}
              {+((Math.min(((page) * pageSize - 1) + 1, totalCityCount)))} out
              of {totalCityCount} total instances.
            </p>
            <p>
              Page {page} of results.
            </p>
            <div className="next-button-container"  >
              <div className="top-buttons" >
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page - 1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  Previous
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page + 1)}
                  disabled={page * pageSize >= totalCityCount}
                  className="substance-pagination"
                >
                  Next
                </Button>
              </div>
              <div className="bottom-buttons" style={{ marginLeft: '1.5rem' }}>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  First
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(Math.ceil(totalCityCount / pageSize))}
                  disabled={page * pageSize >= totalCityCount}
                  className="substance-pagination"
                >
                  Last
                </Button>
              </div>
            </div>
          </div>
          <div className="sub-related-city-cards">
            {Array.isArray(cities) && cities?.map((city, index) => (
              <Card key={index} className="react-card">
                <Link
                  to={`/cities/${slugify(city.name)}`}
                  state={{ city: city }}
                  className="link"
                >
                  <Card.Title>{city.name}</Card.Title>
                  <Card.Text className="mb-2 text-muted">
                    Drug Abuse Statistics
                  </Card.Text>
                  <Card.Body>
                    <ListGroup className="list-group-flush">
                      <ListGroup.Item>Marijuana Use Rate: {city.marijuana} %</ListGroup.Item>
                      <ListGroup.Item>Cocaine Use Rate: {city.cocaine} %</ListGroup.Item>
                      <ListGroup.Item>Heroin Use Rate: {city.heroin} %</ListGroup.Item>
                      <ListGroup.Item>Meth Use Rate: {city.meth} %</ListGroup.Item>
                      <ListGroup.Item>Region: {city.region}</ListGroup.Item>
                    </ListGroup>
                  </Card.Body>
                </Link>
              </Card>
            ))}
          </div>
        </div>
      </div>
      <div className="sub-related-centers">
        <h1 className="sub-related-title">Related Treatment Centers</h1>
        <h5 class="sub-header"><b>These centers treat addiction relating to this drug: </b></h5>
        <div className="sub-centers-rows">
          <div className="pagination-info">
            <p>
              Displaying results {+(page - 1) * pageSize + 1} -{" "}
              {+(Math.min((page) * pageSize - 1, totalCenterCount))} out
              of {+totalCenterCount} total instances.
            </p>
            <p>
              Page {page} of results.
            </p>
            <div className="next-button-container"  >
              <div className="top-buttons" >
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page - 1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  Previous
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(page + 1)}
                  disabled={page * pageSize >= totalCenterCount}
                  className="substance-pagination"
                >
                  Next
                </Button>
              </div>
              <div className="bottom-buttons" style={{ marginLeft: '1.5rem' }}>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(1)}
                  disabled={page === 1}
                  className="substance-pagination"
                >
                  First
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  onClick={() => setPage(Math.ceil(totalCenterCount / pageSize))}
                  disabled={page * pageSize >= totalCenterCount}
                  className="substance-pagination"
                >
                  Last
                </Button>
              </div>
            </div>
          </div>
          <div className="sub-related-center-cards">
            {Array.isArray(centers) && centers.map((center, index) => (
              <Card key={index} className="card">
                <Link
                  to={`/centers/${slugify(center.name1 + center.name2)}`}
                  state={{ center: center }}
                  className="link"
                >
                  <Card.Body>
                    <Card.Title>{center.name1}</Card.Title>
                    <Card.Subtitle className="mb-2 text-muted">
                      {center.name2}
                    </Card.Subtitle>
                    <Card.Text>
                      {center.street1}
                      <br />
                      {center.city}, {center.state} {center.zip}
                      <br />
                      {center.phone}
                      <br />
                      {center.email}
                    </Card.Text>
                  </Card.Body>
                </Link>
              </Card>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}
