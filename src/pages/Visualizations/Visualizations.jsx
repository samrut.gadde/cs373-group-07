import React, { useEffect, useState } from 'react';
import axios from "axios";
import apiLink from "../../api.config";
import CenterVisualization from './CenterVisualization';
import CityVisualization from './CityVisualization';
import SubstanceVisualization from './SubstanceVisualization';
import './Visualizations.css';

// Fetches cities from the API
async function fetchCities(pageSize, page) {
  let cities = [];
  await axios
    .get(apiLink + `/cities?page=${page}&pageSize=${pageSize}`)
    .then((response) => {
      cities = response.data;
    })
    .catch((error) => {
      return [];
    });

  return cities;
}

export default function Visualizations() {
  const [cities, setCities] = useState([]);
  const pageSize = 50;
  const page = 1;

  useEffect(() => {
    setCities([]);
    fetchCities(pageSize, page).then((cities) => {
      setCities(cities);
    });
  }, []);

  return (
    <div className="viz-root">
      <div className="viz-header">
        <h1>Visualizations</h1>
        <p>These visualizations are created using D3.js and React</p>
      </div>

      <div className='viz-container'>
        <SubstanceVisualization/>
        <CenterVisualization width={1000} height={610}/>
        <CityVisualization cities={cities} width={500} height={500}/>
      </div>

      <div className="critiques">
        <h2>Critiques For Our Team</h2>
        <h4>What did we do well?</h4>
        <p>We worked well as a team, and we were able to plan out each phase and meet consistently. We all contributed evenly to the final project and delivered on the parts we were responsible for, but we also checked in with and helped other teammates. Even when things were a little stressful at the end of a phase, we communicated well with each other and were able to get it all done.</p>
        <h4>What did we learn?</h4>
        <p>Overall, we learned how to use various tools to write different types of tests, how to implement a RESTful API and connect it to the frontend, and how to visually design a website. The frontend team learned a lot about React, and the backend team learned a lot about Postgres and Postman.</p>
        <h4>What did we teach each other?</h4>
        <p>A big part of what we learned from working together was communication and asking for help when needed, as well as how to break up a large task and piece each person’s contribution together. Additionally, since the frontend and backend have to be tied together in the end, we had to work with each other to teach each other what we accomplished so everything could be linked correctly. </p>
        <h4>What can we do better?</h4>
        <p>In Phase I and Phase II, some of what we did ended up being put off until close to the due date, so we could’ve started earlier or planned a little better, but this got better with the later phases. Additionally, we could improve some of the design of the website to make it more user-friendly and visually appealing. </p>
        <h4>What effect did the peer reviews have?</h4> 
        <p>They were a helpful way to check in with the rest of the group and make sure we were all on the same page moving into the next phase.</p>
        <h4>What puzzles us?</h4>
        <p>Not much, to be honest, since we feel like we were able to plan out and deliver each part of each phase effectively. </p>
      </div>
    </div>
  );
}
