import React, { useEffect, useRef } from 'react';
import './Visualizations.css';
import axios from "axios";
import apiLink from "../../api.config";
import * as d3 from 'd3'; // Import D3.js library

async function fetchSubstanceNames(filter) {
    let substances = [];
    await axios.get(apiLink + `/substances?category=${filter}`).then((response) => {
        substances = response.data
    })
    let substanceNames = [substances.length];
    for (let i = 0; i < substances.length; i++) {
        let substance = {
            name: substances[i].name,
            value: 1,
        };
        substanceNames[i] = substance;
    }
    // console.log(substanceNames)
    return substanceNames;
}

const SubstanceVisualization = ({ width, height }) => {
    
    const[stimulants, setStimulants] = React.useState([]);
    const[depressants, setDepressants] = React.useState([]);
    const[sedatives, setSedatives] = React.useState([]);
    const[others, setOthers] = React.useState([]);

    React.useEffect(() => {
        fetchSubstanceNames("Stimulant").then((substanceNames) => {
            setStimulants(substanceNames);
        });
        fetchSubstanceNames("Depressant").then((substanceNames) => {
            setDepressants(substanceNames);
        });
        fetchSubstanceNames("Sedative").then((substanceNames) => {
            setSedatives(substanceNames);
        });
        fetchSubstanceNames("Other").then((substanceNames) => {
            setOthers(substanceNames);
        });
    }, []);
    

    let data = {
        name: "flare",
        children: [
            {name: "Stimulants",    children: stimulants},
            {name: "Depressants",   children: depressants},
            {name: "Sedatives",     children: sedatives},
            {name: "Other",         children: others}
        ]
    };
    // console.log(data);

    const ref = useRef(null);

    useEffect(() => {

        //large part of this code taken from d3 examples website: zoomable sunburst
        //https://observablehq.com/@d3/zoomable-sunburst?intent=fork
        const width = 980;
        const height = width;
        const radius = width / 6;

        const color = d3.scaleOrdinal(d3.quantize(d3.interpolateRainbow, data.children.length + 1));

        const hierarchy = d3.hierarchy(data)
            .sum(d => d.value)
            .sort((a, b) => b.value - a.value);
        const root = d3.partition()
            .size([2 * Math.PI, hierarchy.height + 1])
        (hierarchy);
        root.each(d => d.current = d);

        const arc = d3.arc()
            .startAngle(d => d.x0)
            .endAngle(d => d.x1)
            .padAngle(d => Math.min((d.x1 - d.x0) / 2, 0.005))
            .padRadius(radius * 1.5)
            .innerRadius(d => d.y0 * radius)
            .outerRadius(d => Math.max(d.y0 * radius, d.y1 * radius - 1))

        const svg = d3.select(ref.current)
            .attr("viewBox", [-width / 2, -height / 2, width, width])
            .style("font", "13px sans-serif");

        svg.append("text")
            .attr("x", 0)
            .attr("y", height / 2 - 860)
            .attr("text-anchor", "middle")
            .style("font-size", "40px")
            .style("align-self", "center")
            .text("Substances by category, click a category to see more");

        const path = svg.append("g")
        .selectAll("path")
        .data(root.descendants().slice(1))
        .join("path")
            .attr("fill", d => { while (d.depth > 1) d = d.parent; return color(d.data.name); })
            .attr("fill-opacity", d => arcVisible(d.current) ? (d.children ? 0.6 : 0.4) : 0)
            .attr("pointer-events", d => arcVisible(d.current) ? "auto" : "none")

            .attr("d", d => arc(d.current));

        path.filter(d => d.children)
            .style("cursor", "pointer")
            .on("click", clicked);

        const format = d3.format(",d");
        path.append("title")
            .text(d => `${d.ancestors().map(d => d.data.name).reverse().join("/")}\n${format(d.value)}`);

        const label = svg.append("g")
            .attr("pointer-events", "none")
            .attr("text-anchor", "middle")
            .style("user-select", "none")
        .selectAll("text")
        .data(root.descendants().slice(1))
        .join("text")
            .attr("dy", "0.35em")
            .attr("fill-opacity", d => +labelVisible(d.current))
            .attr("transform", d => labelTransform(d.current))
            .text(d => d.data.name);

        const parent = svg.append("circle")
            .datum(root)
            .attr("r", radius)
            .attr("fill", "none")
            .attr("pointer-events", "all")
            .on("click", clicked);

        function clicked(event, p) {
            parent.datum(p.parent || root);

            root.each(d => d.target = {
                x0: Math.max(0, Math.min(1, (d.x0 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
                x1: Math.max(0, Math.min(1, (d.x1 - p.x0) / (p.x1 - p.x0))) * 2 * Math.PI,
                y0: Math.max(0, d.y0 - p.depth),
                y1: Math.max(0, d.y1 - p.depth)
        });

        const t = svg.transition().duration(750);

        path.transition(t)
            .tween("data", d => {
                const i = d3.interpolate(d.current, d.target);
                return t => d.current = i(t);
            })
            .filter(function(d) {
            return +this.getAttribute("fill-opacity") || arcVisible(d.target);
            })
            .attr("fill-opacity", d => arcVisible(d.target) ? (d.children ? 0.6 : 0.4) : 0)
            .attr("pointer-events", d => arcVisible(d.target) ? "auto" : "none") 

            .attrTween("d", d => () => arc(d.current));

        label.filter(function(d) {
            return +this.getAttribute("fill-opacity") || labelVisible(d.target);
            }).transition(t)
            .attr("fill-opacity", d => +labelVisible(d.target))
            .attrTween("transform", d => () => labelTransform(d.current));
        }
        
        function arcVisible(d) {
            console.log(d.y1, d.y0, d.x1, d.x0);
            return d.y1 <= 2 && d.y0 >= 1 && d.x1 > d.x0;
        }

        function labelVisible(d) {
            return d.y1 <= 2 && d.y0 >= 1 && (d.y1 - d.y0) * (d.x1 - d.x0) > 0.03;
        }

        function labelTransform(d) {
            const x = (d.x0 + d.x1) / 2 * 180 / Math.PI;
            const y = (d.y0 + d.y1) / 2 * radius;
            return `rotate(${x - 90}) translate(${y},0) rotate(${x < 180 ? 0 : 180})`;
        }

        return () => {
            svg.selectAll("*").remove();
        }
    });

    return <svg ref = {ref} width={width} height={height}/>;
}

export default SubstanceVisualization;