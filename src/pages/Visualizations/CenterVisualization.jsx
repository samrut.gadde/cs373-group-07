import React, { useEffect } from "react";
import * as d3 from "d3";
import world from "./world.json";
import apiLink from "../../api.config";
import axios from "axios";

export default function CenterVisualization({ width, height }) {
  async function fetchTreatmentCenters() {
    if (localStorage.getItem("treatmentCenters")) {
      console.log("Using cached treatment centers");
      return JSON.parse(localStorage.getItem("treatmentCenters"));
    }

    let treatmentCenters = [];
    let currentPage = 1;

    let totalTreatmentCenters = await axios
      .get(apiLink + "/centers/count")
      .then((response) => {
        return response.data.count;
      })
      .catch((error) => {
        return 0;
      });

    while (treatmentCenters.length < totalTreatmentCenters) {
      let response = await axios
        .get(apiLink + `/centers?page=${currentPage}&pageSize=500`)
        .then((response) => {
          return response.data;
        })
        .catch((error) => {
          return [];
        });

      treatmentCenters = treatmentCenters.concat(response);
      currentPage++;
    }

    // filter out only longitudes and latitudes
    treatmentCenters = treatmentCenters.map((center) => {
      return {
        longitude: center.longitude,
        latitude: center.latitude,
      };
    });

    localStorage.setItem("treatmentCenters", JSON.stringify(treatmentCenters));
    return treatmentCenters;
  }

  useEffect(() => {
    const svg = d3.select("#treatment-centers-map");

    // Map and projection
    const projection = d3.geoAlbersUsa();
    const path = d3.geoPath().projection(projection);

    async function fetchData() {
      const treatmentCenters = await fetchTreatmentCenters();
      const data = world.features.filter((d) => d.properties.name == "USA");

      svg
        .append("text")
        .attr("x", width / 2)
        .attr("y", height / 2 - 300)
        .attr("text-anchor", "middle")
        .style("font-size", "40px")
        .style("align-self", "center")
        .text("Treatment Centers in the USA");

      // Draw the map
      svg
        .append("g")
        .selectAll("path")
        .data(data)
        .enter()
        .append("path")
        .attr("fill", "#b8b8b8")
        .attr("d", path)
        .attr("class", "state");

      // Draw the treatment centers
      svg
        .append("g")
        .selectAll("circle")
        .data(treatmentCenters)
        .join("circle")
        .attr("transform", (d) => {
          let p = projection([d.longitude, d.latitude]);
          return p ? `translate(${p[0]}, ${p[1]})` : "";
        })
        .attr("r", 2)
        .attr("class", "location");
    }

    fetchData();
  }, []);

  return (
    <svg id="treatment-centers-map" viewBox={`0 0 ${width} ${height}`} ></svg>
  );
}
