import React, { useEffect } from "react";
import * as d3 from "d3";

const HealthVisualization = ({ healthCenters, width, height }) => {
  useEffect(() => {
    if (healthCenters.length === 0) return;

    const categoryCounts = healthCenters.reduce((acc, center) => {
      acc[center.facility_type] = (acc[center.facility_type] || 0) + 1;
      return acc;
    }, {});

    const categories = Object.keys(categoryCounts);
    const counts = Object.values(categoryCounts);

    // d3.select("#health-dataviz").select("svg").remove();

    const margin = { top: 60, right: 30, bottom: 90, left: 40 };
    const width = 460 - margin.left - margin.right;
    const height = 450 - margin.top - margin.bottom;

    const svg = d3
      .select("#health-dataviz")
      .append("svg")
      .attr("width", width + margin.left + margin.right)
      .attr("height", height + margin.top + margin.bottom)
      .append("g")
      .attr("transform", `translate(${margin.left},${margin.top})`);

    const x = d3.scaleBand().range([0, width]).domain(categories).padding(0.2);

    svg
      .append("g")
      .attr("transform", `translate(0, ${height})`)
      .call(d3.axisBottom(x))
      .selectAll("text")
      .attr("transform", "translate(-10,0)rotate(-45)")
      .style("text-anchor", "end");

    const y = d3
      .scaleLinear()
      .domain([0, d3.max(counts)])
      .range([height, 0]);

    svg.append("g").call(d3.axisLeft(y));

    svg
      .selectAll("rect")
      .data(categories)
      .enter()
      .append("rect")
      .attr("x", (d) => x(d))
      .attr("width", x.bandwidth())
      .attr("fill", "#69b3a2")
      .attr("height", (d) => height - y(categoryCounts[d]))
      .attr("y", (d) => y(categoryCounts[d]));

    svg
      .selectAll("rect")
      .transition()
      .duration(800)
      .attr("y", (d) => y(categoryCounts[d]))
      .attr("height", (d) => height - y(categoryCounts[d]))
      .delay((d, i) => i * 100);
  }, [healthCenters]);

  return (
    <div className="health-viz-container">
      <h1>Facility Types of Health Centers</h1>
      <p>Number of health centers by facility type</p>
      <svg
        id="health-dataviz"
        height={height}
        width={width}
        viewBox={`0 0 ${width} ${height}`}
      ></svg>
    </div>
  );
};

export default HealthVisualization;
