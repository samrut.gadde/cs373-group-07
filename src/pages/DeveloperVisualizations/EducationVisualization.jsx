import React, { useEffect, useRef } from "react";
import axios from "axios";
import * as d3 from "d3";
import texas from "./texas.json";

export default function EducationVisualization({ width, height }) {
  const ref = useRef();

  async function fetchEducationData() {
    let locations = [];

    await axios
      .get("https://api.veteranhaven.me/education")
      .then((response) => {
        response.data.education.forEach((d) => {
          locations.push({
            name: d.name,
            lat: parseFloat(d.latitude),
            lon: parseFloat(d.longitude),
          });
        });
      })
      .catch((error) => {
        // console.log(error);
        return [];
      });

    return locations;
  }

  useEffect(() => {
    const svg = d3.select("#education-map");
    const layoutWidth = svg.node().getBoundingClientRect().width;
    const layoutHeight = svg.node().getBoundingClientRect().height;
    console.log("layoutWidth", layoutWidth);
    var projection = d3
      .geoMercator()
      .fitSize([layoutWidth, layoutHeight], texas);
    const path = d3.geoPath().projection(projection);

    async function fetchData() {
      const locations = await fetchEducationData();

      svg
        .selectAll("path")
        .data(texas.features)
        .enter()
        .append("path")
        .attr("d", path)
        .attr("class", "texas-map");

      svg
        .append("g")
        .selectAll("circle")
        .data(locations)
        .join("circle")
        .attr("transform", (d) => {
          let p = projection([d.lon, d.lat]);
          return p ? `translate(${p[0]}, ${p[1]})` : "";
        })
        .attr("r", 2)
        .attr("class", "location");
    }

    fetchData();
  }, []);

  return (
    <div className="edu-map-container">
      <h1>Education Centers in Texas</h1>
      <svg
        height={height}
        width={width}
        viewBox={`0 0 ${width} ${height}`}
        id="education-map"
      ></svg>
    </div>

  );
}
