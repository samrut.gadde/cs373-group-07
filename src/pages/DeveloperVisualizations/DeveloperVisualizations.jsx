import React, { useEffect } from 'react'
import { useState } from 'react';
import axios from "axios";
import HealthVisualization from './HealthVisualization';
import HousingVisualization from './HousingVisualization';
import EducationVisualization from './EducationVisualization';
import './DeveloperVisualizations.css'

// gets the health centers from developer's API
async function fetchHealthCenters() {
  let healthCenters = [];
  await axios
    .get('https://api.veteranhaven.me/healthcare')
    .then((response) => {
      healthCenters = response.data.healthcare;
    })
    .catch((error) => {
      return [];
    });

  return healthCenters;
}

export default function DeveloperVisualizations() {
  const [healthCenters, setHealthCenters] = useState([]);

  useEffect(() => {
    setHealthCenters([]);
    fetchHealthCenters().then((healthCenters) => {
      setHealthCenters(healthCenters);
    });
  }, []);

  return (
    <div className="dev-viz-root">
      <div className="dev-viz-header">
        <h1>Visualizations</h1>
        <p>These visualizations are created using D3.js and React</p>
      </div>

      <div className='dev-viz-container'>
        <HealthVisualization healthCenters={healthCenters} height={500} width={500}/>
        <EducationVisualization height={500} width={500}/>
        <h3>Housing Eligibility Type Pie Chart: Hover over chart for labels</h3>
        <HousingVisualization width={1000} height={610}/>
      </div>

      <div className="critiques">
        <h2>Critiques For Our Developer Team</h2>
        <h4>What did they do well?</h4>
        <p>They have a very clean-looking and user-friendly website that is easy to navigate, and they responded to our user stories effectively. Their splash page is also well-designed, and it makes the website look very inviting. </p>
        <h4>How effective was their RESTful API?</h4>
        <p>Overall, it was pretty effective. One minor critique we did have, though, was that you can’t request specific page sizes when requesting a page. There's also no documentation on how to perform filtered/sorted API calls.</p>
        <h4>How well did they implement your user stories?</h4>
        <p>Well, since most of what we mentioned was related to responsiveness/visual inconsistencies, which were fixed by the start of the next phase. Everything was responded to and completed effectively.</p>
        <h4>What did we learn from their website?</h4>
        <p>It was nice to look at another website for design ideas since they approached their frontend quite differently from how we approached ours. Additionally, in terms of content, it was very educational and provided detailed information on a variety of different resources for veterans and links on where to get more information.</p>
        <h4>What can they do better?</h4>
        <p>This is quite minor, but there could be some small tweaks to the font color/weight on certain parts of the website to make it slightly more readable. Besides that, the website is very well made. </p>
        <h4>What puzzles us about their website?</h4>
        <p>Not much, since it communicates information about resources for veterans in a clean and concise way, and as mentioned before, it’s user-friendly. </p>
      </div>
    </div>
  );
}
