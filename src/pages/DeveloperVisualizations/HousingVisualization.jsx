import React, { useEffect, useRef } from 'react';
import axios from "axios";
import * as d3 from 'd3'; // Import D3.js library
import './HousingVisualization.css';

//api call initially used to fetch data but it ended up being expensive/slow so just stored the data locally
async function fetchHousingData() {
    let housingData = {};
    await axios.get("https://api.veteranhaven.me/housing/visualize").then((response) => {
        housingData = response.data
    })
    return housingData;
}

const HousingVisualization = ({ width, height }) => {

    // const[housing, setHousing] = React.useState({});

    // React.useEffect(() => {
    //     fetchHousingData().then((housingData) => {
    //         setHousing(housingData);
    //     });
    // });

    const housing = {
        "Adults": 70,
        "All": 24,
        "Disabled": 18,
        "LowIncome": 140,
        "Other": 26,
        "Residents": 34,
        "Seniors": 1,
        "Veterans": 1
    };

    const data = Object.entries(housing).map(([key, value]) => ({ key, value }));

    const ref = useRef(null);

    useEffect(() => {

        const radius = Math.min(width, height) / 2,
        outerRadius = radius - 10;
        const cornerRadius = 6;
        const innerRadius = 85;
        const padAngle = 0.001;
        const margin = 50;

        const svg = d3.select(ref.current);

        //Portions of code used from d3 examples website: pie chart
        //https://observablehq.com/@marialuisacp/pie-chart
        svg.attr('width', width + margin + margin)
            .attr('height', height + margin + margin)

        const color = d3.scaleOrdinal()
            .domain(data)
            .range(['#FF6961', '#FFB480', '#F8F38D', '#42D6A4', '#08CAD1', '#F9ADF6', '#9D94FF', '#C780E8'])
        
        console.log(data);

        const g = svg.append('g')
            .attr('transform', 'translate(' + width / 2 + ',' + height / 2 + ')')

        const pie = d3.pie()
            .value((d) => d.value)

        console.log(pie);

        const arc = d3.arc()
            .innerRadius(innerRadius)
            .outerRadius(outerRadius)
            .cornerRadius(cornerRadius)
            .padAngle(padAngle)

        const part = g.selectAll('.part')
            .data(pie(Object.values(data)))
            .enter()
            .append('g')

        part.append('path')
            .attr('d', arc)
            .attr('fill', (d, i) => color(i))

        part.append("text")
            .attr('transform', (d) => 'translate(' + arc.centroid(d) + ')')
            .text((d) => d.data.key)
            .attr('class', "devText")
            .attr('fill', 'white')
            
            

        return () => {
            svg.selectAll("*").remove();
        }
  
    });

    return <svg ref = {ref} width={width} height={height}/>;
}

export default HousingVisualization;