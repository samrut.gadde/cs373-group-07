import { useState, useEffect } from "react";
import { Card, Button, ListGroup } from "react-bootstrap";
import slugify from "react-slugify";
import { Link } from "react-router-dom";
import { useSearchParams } from "react-router-dom";
import "./Search.css";
import axios from "axios";
import apiLink from "../../api.config";

// Highlights search term in the results
const highlightSearchTermCenters = (text, searchTerm) => {
  if (!text || !searchTerm) return null;
  if (searchTerm.length > 0) {
    const regex = new RegExp(`(${searchTerm})`, "gi");
    const parts = text.split(regex);
    return parts.map((part, index) =>
      regex.test(part) ? (
        <span key={index} className="highlight">
          {part}
        </span>
      ) : (
        part
      )
    );
  } else {
    return text;
  }
};

// gets the highlighted term in the results
const SearchResult = ({ text, searchTerm }) => {
  return <div>{highlightSearchTermCenters(text, searchTerm)}</div>;
};

// gets the number of treatment centers in the search result
async function fetchTreatmentCenterCount(searchQuery) {
  let count = 0;
  let baseQuery = `/centers/count?`;
  if (searchQuery) {
    baseQuery += `query=${searchQuery}`;
  }

  await axios.get(apiLink + baseQuery).then((response) => {
    count = response.data.count;
  });
  return count;
}

// gets the number of substances in the search result
async function fetchSubstanceCount(searchQuery) {
  let count = 0;
  if (searchQuery) {
    await axios
      .get(apiLink + `/substances/count?query=${searchQuery}`)
      .then((response) => {
        count = response.data.count;
      })
      .catch((error) => {
        return 0;
      });
  } else {
    await axios
      .get(apiLink + "/substances/count")
      .then((response) => {
        count = response.data.count;
      })
      .catch((error) => {
        return 0;
      });
  }

  return count;
}

// gets the number of cities in the search result
async function fetchCityCount(searchQuery) {
  let count = 0;
  await axios
    .get(apiLink + "/cities/count")
    .then((response) => {
      count = response.data.count;
    })
    .catch((error) => {
    });
  return count;
}

// gets the counts for each type of model in the search result
async function fetchCounts(searchQuery) {
  const treatmentCenterCount = await fetchTreatmentCenterCount(searchQuery);
  const substanceCount = await fetchSubstanceCount(searchQuery);
  const cityCount = await fetchCityCount(searchQuery);
  const totalCount =
    parseInt(treatmentCenterCount) +
    parseInt(substanceCount)

  return { treatmentCenterCount, substanceCount, cityCount, totalCount };
}

// gets the centers w the search term
async function fetchTreatmentCenters(searchQuery, page, pageSize) {
  let centers = [];
  let baseQuery = `/centers?page=${page ?? 1}&pageSize=${pageSize ?? 20}`;
  if (searchQuery) {
    baseQuery += `&query=${searchQuery}`;
  }

  await axios
    .get(apiLink + baseQuery)
    .then((response) => {
      centers = response.data;
    })
    .catch((error) => {
      return [];
    });
  return centers;
}

// gets the substances w the search term
async function fetchSubstances(searchQuery, page, pageSize) {
  let substances = [];
  if (searchQuery) {
    await axios
      .get(
        apiLink +
        `/substances?page=${page}&pageSize=${pageSize}&query=${searchQuery}`
      )
      .then((response) => {
        substances = response.data;
      })
      .catch((error) => {
        return [];
      });
  } else {
    await axios
      .get(apiLink + `/substances?page=${page}&pageSize=${pageSize}`)
      .then((response) => {
        substances = response.data;
      })
      .catch((error) => {
        return [];
      });
  }
  return substances;
}

// gets the cities w the search term
async function fetchCities(searchQuery, page, pageSize) {
  let cities = [];
  if (!searchQuery) {
    return [];
  }

  await axios
    .get(
      apiLink +
      `/cities/queryCities?page=${page}&pageSize=${pageSize}&query=${searchQuery}`
    )
    .then((response) => {
      cities = response.data;
    })
    .catch((error) => {
      return [];
    });
  return cities;
}

// gets all results matching the search term 
async function fetchAll(searchQuery, page, pageSize) {
  let { treatmentCenterCount, substanceCount, totalCount } =
    await fetchCounts(searchQuery);
  console.log(searchQuery);
  const treatmentCenters = await fetchTreatmentCenters(
    searchQuery,
    page,
    pageSize
  );
  const substances = await fetchSubstances(searchQuery, page, pageSize);
  const cities = await fetchCities(searchQuery, page, pageSize);
  let cityCount = cities.length;
  totalCount += cityCount;

  return {
    treatmentCenterCount,
    substanceCount,
    cityCount,
    totalCount,
    treatmentCenters,
    substances,
    cities,
  };
}

// displays the centers as cards
function CenterCard({ center, searchQuery, index }) {
  if (!center) return null;
  return (
    <Card key={index} className="card">
      <Link
        to={`/centers/${slugify(center.name1 + center.name2)}`}
        state={{ center: center }}
        className="link"
      >
        <Card.Body>
          <Card.Title>
            <SearchResult text={center.name1} searchTerm={searchQuery} />
          </Card.Title>
          <Card.Subtitle>
            <SearchResult text={center.name2} searchTerm={searchQuery} />
          </Card.Subtitle>
          <Card.Text>
            <SearchResult
              text={`${center.street1}, ${center.city}, ${center.state}, ${center.zip}`}
              searchTerm={searchQuery}
            />
            <br />
            <SearchResult text={center.phone} searchTerm={searchQuery} />
            <br />
            <SearchResult text={center.email} searchTerm={searchQuery} />
          </Card.Text>
        </Card.Body>
      </Link>
    </Card>
  );
}

// displays the substances as cards
function SubstanceCard({ substance, searchQuery, index }) {
  return (
    <Card key={index} className="card">
      <Link
        to={`/substances/${slugify(substance.name)}`}
        state={{ substance: substance }}
        className="link"
      >
        <Card.Body>
          <Card.Title>
            <SearchResult text={substance.name} searchTerm={searchQuery} />
          </Card.Title>
          <Card.Subtitle>
            <SearchResult text={substance.category} searchTerm={searchQuery} />
          </Card.Subtitle>
          <Card.Text>
            <br />
            <SearchResult
              text={substance.description.substring(0, 200) + "..."}
              searchTerm={searchQuery}
            />
          </Card.Text>
        </Card.Body>
      </Link>
    </Card>
  );
}

// displays the cities as cards
function CityCard({ city, searchQuery, index }) {
  return (
    <Card key={index} className="react-card">
      <Link
        to={`/cities/${slugify(city.name)}`}
        state={{ city: city }}
        className="link"
      >
        <Card.Title>
          <SearchResult text={city.name} searchTerm={searchQuery} />
        </Card.Title>
        <Card.Text className="mb-2 text-muted">Drug Abuse Statistics</Card.Text>
        <Card.Body>
          <ListGroup className="list-group-flush">
            <ListGroup.Item>Marijuana: {city.marijuana}%</ListGroup.Item>
            <ListGroup.Item>Cocaine: {city.cocaine}%</ListGroup.Item>
            <ListGroup.Item>Heroin: {city.heroin}%</ListGroup.Item>
            <ListGroup.Item>Meth: {city.meth}%</ListGroup.Item>
            <ListGroup.Item>Region: {city.region}</ListGroup.Item>
          </ListGroup>
        </Card.Body>
      </Link>
    </Card>
  );
}

// displays all sets of cards for the search results
function DisplayCards({ treatmentCenters, substances, cities, searchQuery }) {
  return (
    <div className="glob-search-card-container">
      <div className="glob-search-inner-card-container">
        {treatmentCenters.length !== 0 ?
          treatmentCenters.map((center, index) => (
            <CenterCard
              center={center}
              searchQuery={searchQuery}
              index={index}
              key={index}
            />
          )) :
          <Card>
            <Card.Body>
              <Card.Title>No Treatment Centers Found</Card.Title>
            </Card.Body>
          </Card>
        }
      </div>
      <div className="glob-search-inner-card-container">
        {substances.length !== 0 ?
          substances.map((substance, index) => (
            <SubstanceCard
              substance={substance}
              searchQuery={searchQuery}
              index={index}
              key={index}
            />
          ))
          :
          <Card>
            <Card.Body>
              <Card.Title>No Substances Found</Card.Title>
            </Card.Body>
          </Card>
        }
      </div>
      <div className="glob-search-inner-card-container">
        {cities.length !== 0 ?
          cities.map((city, index) => (
            <CityCard
              city={city}
              searchQuery={searchQuery}
              index={index}
              key={index}
            />
          )) :
          <Card className="react-card">
            <Card.Body>
              <Card.Title>No Cities Found</Card.Title>
            </Card.Body>
          </Card>
        }
      </div>
    </div>
  );
}

export default function Search() {
  const [treatmentCenters, setTreatmentCenters] = useState([]);
  const [substances, setSubstances] = useState([]);
  const [cities, setCities] = useState([]);
  const [treatmentCenterCount, setTreatmentCenterCount] = useState(0);
  const [substanceCount, setSubstanceCount] = useState(0);
  const [cityCount, setCityCount] = useState(0);
  const [totalCount, setTotalCount] = useState(0);
  const [page, setPage] = useState(1);
  const pageSize = 5;
  const [searchParams, setSearchParams] = useSearchParams();
  const [searchQuery, setSearchQuery] = useState(
    searchParams.get("query") || ""
  );

  // returns nothing if there's an empty search 
  useEffect(() => {
    if (!searchQuery) {
      setTreatmentCenters([]);
      setSubstances([]);
      setCities([]);
      setTotalCount(0);
      return;
    }

    // on a delay, gets the instances/counts using the search query
    const delayDebounceFn = setTimeout(async () => {
      const {
        treatmentCenterCount,
        substanceCount,
        cityCount,
        totalCount,
        treatmentCenters,
        substances,
        cities,
      } = await fetchAll(searchQuery, page, pageSize);

      setTotalCount(totalCount);
      setTreatmentCenterCount(treatmentCenterCount);
      setSubstanceCount(substanceCount);
      setCityCount(cityCount);
      setTreatmentCenters(treatmentCenters);
      setSubstances(substances);
      setCities(cities);
    }, 300);

    return () => clearTimeout(delayDebounceFn);
  }, [searchQuery, page]);

  // moves to a new search page when the search bar is used
  function handleSearchChange(e) {
    setSearchQuery(e.target.value);
    window.history.replaceState(null, "", `/search?query=${e.target.value}`)
  }

  // tracks the total number of instances and the count on this page
  function instanceCounter(totalCount, page, pageSize) {
    if (Math.ceil(totalCount / pageSize) >= page) {
      return ("Instances " + ((page - 1) * pageSize + 1) + " through " + page * pageSize)
    }
    else {
      return "No more instances"
    }
  }

  // tracks the total number of pages and the current one
  function pageCounter(page, treatmentCenterCount, substanceCount, cityCount, pageSize) {
    if ((Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize) == 0) {
      return "Page 0 of 0."
    }
    if (page > Math.ceil(Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize)) {
      return ("Page " + Math.ceil(Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize) + " of " + Math.ceil(Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize)) + " ."
    }
    return ("Page " + page + " of " + Math.ceil(Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize)) + " ."
  }

  return (
    <div className="glob-search-container">
      <div className="glob-search-header">
        <h1>Search</h1>
        <input
          type="text"
          placeholder="Search..."
          defaultValue={searchParams.get("query") || ""}
          onChange={handleSearchChange}
          className="search-bar rounded-pill"
        />
      </div>
      <div className="glob-search-content-container">
        <div className="sub-sort-container">
          <p>
            {+totalCount + 1} total instances
          </p>
          <p> {pageCounter(page, treatmentCenterCount, substanceCount, cityCount, pageSize)}</p>
          <p> Displaying Treatment Centers: {instanceCounter(treatmentCenterCount, page, pageSize)}</p>
          <p> Displaying Substances: {instanceCounter(substanceCount, page, pageSize)}</p>
          <p> Displaying Cities: {instanceCounter(cityCount, page, pageSize)}</p>
          <div className="next-button-container">
            <div className="top-buttons">
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(page - 1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                Previous
              </Button>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(page + 1)}
                disabled={page * pageSize >= Math.max(treatmentCenterCount, substanceCount, cityCount)}
                className="substance-pagination"
              >
                Next
              </Button>
            </div>
            <div className="buttom-buttons" style={{ marginLeft: "1.5rem" }}>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                First
              </Button>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(Math.ceil(Math.max(treatmentCenterCount, substanceCount, cityCount) / pageSize))}
                disabled={page * pageSize >= Math.max(treatmentCenterCount, substanceCount, cityCount)}
                className="substance-pagination"
              >
                Last
              </Button>
            </div>
          </div>
        </div>
        <DisplayCards
          treatmentCenters={treatmentCenters}
          substances={substances}
          cities={cities}
          searchQuery={searchQuery}
        />
      </div>
    </div>
  );
}
