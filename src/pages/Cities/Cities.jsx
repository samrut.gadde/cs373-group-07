import React, { useState, useEffect } from "react";
import ListGroup from "react-bootstrap/ListGroup";
import Card from "react-bootstrap/Card";
import { Button } from "react-bootstrap";
import { Link } from "react-router-dom";
import slugify from "react-slugify";
import axios from "axios";
import apiLink from "../../api.config";
import "./Cities.css";

// Fetches the cities count from the API
async function fetchCitiesCount() {
  let count = 0;
  await axios
    .get(apiLink + "/cities/count")
    .then((response) => {
      count = response.data.count;
    });
  return count;
}

// Fetches the matching cities based on the search query using the API
async function fetchCitySearch(pageSize, page, searchQuery) {
  let cities = [];

  await axios
    .get(
      apiLink +
      `/cities/queryCities?page=${page}&pageSize=${pageSize}&query=${searchQuery}`
    )
    .then((response) => {
      cities = response.data;
    })
    .catch((error) => {
      return [];
    });

  return cities;
}

// Fetches the cities sorted/filtered by what the user specifies using the API
async function fetchSortedFilteredCities(
  pageSize,
  page,
  filterColumn,
  filterVal,
  sortColumn,
  sortDirection
) {
  let cities = [];
  // set up api endpoint based on if/which sort/filtering options selected
  let apiEndpoint =
    apiLink + `/cities/sortFilterCities?`;
  if (sortColumn !== "None") {
    apiEndpoint += `sortColumn=${sortColumn}&sortDirection=${sortDirection}`;
  }
  if (filterColumn !== "None") {
    apiEndpoint += `${sortColumn !== "None" ? "&" : ""}filterColumn=${filterColumn}&filterVal=${filterVal}`;
  }
  apiEndpoint += `&page=${page}&pageSize=${pageSize}`;

  // Fetch data
  const response = await axios.get(apiEndpoint);
  cities = response.data;
  return cities;
}

// gets all cities from the API
async function fetchCities(pageSize, page) {
  let cities = [];

  await axios
    .get(apiLink + `/cities?page=${page}&pageSize=${pageSize}`)
    .then((response) => {
      cities = response.data;
    })
    .catch((error) => {
      return [];
    });

  return cities;
}

export default function Cities() {
  const pageSize = 20;
  const [page, setPage] = useState(1);
  const [cities, setCities] = useState([]);
  const [totalCityCount, setTotalCityCount] = useState(0);
  const [searchQuery, setSearchQuery] = useState("");
  const [filterColumn, setFilterColumn] = useState("None");
  const [filterValue, setFilterValue] = useState("None");
  const [sortColumn, setSortColumn] = useState("None");
  const [sortDirection, setSortDirection] = useState("ascending");

  // highlights the search term in the results
  const SearchResult = ({ text, searchTerm }) => {
    return <div>{highlightSearchTerm(text, searchTerm)}</div>;
  };

  // Fetches the cities from the API on page load
  useEffect(() => {
    setCities([]);
    fetchCitiesCount().then((count) => {
      setTotalCityCount(count);
    });

    fetchCities(pageSize, page).then((cities) => {
      setCities(cities);
    });
  }, []);

  // Updates the cities displayed as the page changes
  useEffect(() => {
    const debounce = setTimeout(() => {
      if (searchQuery !== "") {
        fetchCitySearch(pageSize, page, searchQuery).then((cities) => {
          setCities(cities);
          setTotalCityCount(cities.length);
        });
      } else {
        fetchCities(pageSize, page).then((cities) => {
          setCities(cities);
        });
      }
    }, 500);

    return () => clearTimeout(debounce);
  }, [page, searchQuery]);

  // Sorts using the selected filters
  const applySortFilters = () => {
    // make api call using react hook variables
    fetchSortedFilteredCities(
      pageSize,
      page,
      filterColumn,
      filterValue,
      sortColumn,
      sortDirection
    ).then((cities) => {
      setCities(cities);
      setTotalCityCount(cities.length);
    });
  };

  // returns to displaying all cities
  const clearSortFilters = () => {
    setFilterColumn("None");
    setSortColumn("None");
    setSortDirection("ascending");
    setFilterValue("None");
    fetchCities(pageSize, page).then((cities) => {
      setCities(cities);
    });
    fetchCitiesCount().then((count) => {
      setTotalCityCount(count);
    });
  };

  // updates variable to store selected search term
  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  // updates variable to store selected filters
  const handleFilterChange = (e) => {
    setFilterColumn(e.target.value);
  };

  // updates variable to store selected sort values
  const handleSortChange = (e) => {
    setSortColumn(e.target.value);
  };

  // updates page counter based on the current page
  function pageCounter(page, pageSize, totalCityCount) {
    if (totalCityCount === 0) {
      return "Displaying results 0 - 0.";
    }
    return (
      "Displaying results " +
      (page - 1) * pageSize +
      " - " +
      Math.min(page * pageSize - 1, totalCityCount) +
      " out of " +
      totalCityCount +
      " instances."
    );
  }

  return (
    <>
      <div className="cities-container">
        <div className="sub-header">
          <h1>City Statistics</h1>
          <p>
            There are several cities that may have higher usages of certain
            substances and thus may have more at-risk populations. Click on a card
            to learn more.
          </p>
          <p>
            Note: substance use statistics reflect the % of residents who have
            used the substance at least once.
          </p>

          <input
            type="text"
            placeholder="Search..."
            defaultValue={searchQuery}
            onChange={handleSearchChange}
            className="search-bar rounded-pill"
          />
        </div>
        <div className="sub-content-container">
          <div className="sub-sort-container">
            <p>{pageCounter(page, pageSize, totalCityCount)}</p>
            <p>Page {page} of results.</p>
            <FilterMenu
              onFilterChange={handleFilterChange}
              filterColumn={filterColumn}
              setFilterColumn={setFilterColumn}
              filterValue={filterValue}
              setFilterValue={setFilterValue}
            />
            <SortMenu
              onSortChange={handleSortChange}
              sortColumn={sortColumn}
              setSortColumn={setSortColumn}
            />
            <div className="sort-buttons">
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                className="cities-button"
                onClick={() => setSortDirection("ascending")}
              >
                Ascending
              </Button>
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                className="cities-button"
                onClick={() => setSortDirection("descending")}
              >
                Descending
              </Button>
            </div>

            <div className="top-buttons">
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                className="cities-button"
                onClick={() => applySortFilters()}
              >
                Apply Sort and Filters
              </Button>
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                className="cities-button"
                onClick={() => clearSortFilters()}
              >
                Clear Sort and Filters
              </Button>
            </div>

            <div className="next-button-container">
              <div className="top-buttons">
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  className="cities-button"
                  onClick={() => setPage(page - 1)}
                  disabled={page === 1}
                >
                  Previous
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  className="cities-button"
                  onClick={() => setPage(page + 1)}
                  disabled={page * pageSize >= totalCityCount}
                >
                  Next
                </Button>
              </div>
              <div
                className="bottom-buttons"
                style={{ marginLeft: "1.5rem" }}
              >
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  className="cities-button"
                  onClick={() => setPage(1)}
                  disabled={page === 1}
                >
                  First
                </Button>
                <Button
                  style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                  className="cities-button"
                  onClick={() =>
                    setPage(Math.ceil(totalCityCount / pageSize))
                  }
                  disabled={page * pageSize >= totalCityCount}
                >
                  Last
                </Button>
              </div>
            </div>
          </div>
          <div className="sub-card-container">
            {cities.length !== 0 ? (
              cities.map((city, index) => {
                return (
                  <Card key={index} className="card">
                    <Link
                      to={`/cities/${slugify(city.name)}`}
                      state={{ city: city }}
                      className="link"
                    >
                      <Card.Body>
                        <Card.Title>
                          <SearchResult
                            text={city.name}
                            searchTerm={searchQuery}
                          />
                        </Card.Title>
                        <Card.Text className="mb-2 text-muted">
                          Drug Abuse Statistics
                        </Card.Text>

                        <ListGroup className="list-group-flush">
                          <ListGroup.Item>
                            Marijuana: {city.marijuana}%
                          </ListGroup.Item>
                          <ListGroup.Item>
                            Cocaine: {city.cocaine}%
                          </ListGroup.Item>
                          <ListGroup.Item>
                            Heroin: {city.heroin}%
                          </ListGroup.Item>
                          <ListGroup.Item>Meth: {city.meth}%</ListGroup.Item>
                          <ListGroup.Item>
                            Region: {city.region}
                          </ListGroup.Item>
                        </ListGroup>
                      </Card.Body>
                    </Link>
                  </Card>
                );
              })
            ) : (
              <div>No Cities Available.</div>
            )}
          </div>
        </div>
      </div>
    </>
  );
}

// Highlights the search term using the specified CSS
const highlightSearchTerm = (text, searchTerm) => {
  if (searchTerm.length > 0) {
    const regex = new RegExp(`(${searchTerm})`, "gi");
    const parts = text.split(regex);
    return parts.map((part, index) =>
      regex.test(part) ? (
        <span key={index} className="highlight">
          {part}
        </span>
      ) : (
        part
      )
    );
  }
  // no search term entered, do nothing
  else {
    return text;
  }
};

// displays information for filtering
const FilterMenu = ({
  onFilterChange,
  filterColumn,
  setFilterColumn,
  filterValue,
  setFilterValue,
}) => {
  const handleFilterChange = (e) => {
    setFilterColumn(e.target.value);
    if (onFilterChange) {
      onFilterChange(e.target.value);
    }
  };
  // sets up option to specify more information when filter is selected
  let cityButtons = null;
  if (
    filterColumn == "city" ||
    filterColumn == "state" ||
    filterColumn == "marijuana" ||
    filterColumn == "meth" ||
    filterColumn == "cocaine" ||
    filterColumn == "heroin"
  ) {
    cityButtons = (
      <div>
        <input
          type="text"
          placeholder="Enter filter value"
          value={filterValue}
          onChange={(e) => setFilterValue(e.target.value)}
          className="filter-input"
        />
      </div>
    );
  }

  return (
    <div className="filter-container">
      <label htmlFor="filterOptions" className="filter-label">
        Filter By: 
      </label>
      <select
        id="filterOptions"
        value={filterColumn}
        onChange={handleFilterChange}
        className="filter-select"
      >
        <option value="">Select an option</option>
        <option value="city">City Name</option>
        <option value="state">State Name</option>
        <option value="marijuana">Marijuana Use</option>
        <option value="cocaine">Cocaine Use</option>
        <option value="heroin">Heroin Use</option>
        <option value="meth">Meth Use</option>
      </select>
      {cityButtons}
    </div>
  );
};

// Displays all information for sorting
const SortMenu = ({ onSortChange, sortColumn, setSortColumn }) => {
  const handleSortChange = (e) => {
    setSortColumn(e.target.value);
    if (onSortChange) {
      onSortChange(e.target.value);
    }
  };

  return (
    <div className="sort-container">
      <label htmlFor="sortOptions" className="sort-label">
        Sort By: 
      </label>
      <select
        id="sortOptions"
        value={sortColumn}
        onChange={handleSortChange}
        className="sort-select"
      >
        <option value="">Select an option</option>
        <option value="city">City Name</option>
        <option value="state">State Name</option>
        <option value="marijuana">Marijuana Use</option>
        <option value="cocaine">Cocaine Use</option>
        <option value="heroin">Heroin Use</option>
        <option value="meth">Meth Use</option>
      </select>
    </div>
  );
};
