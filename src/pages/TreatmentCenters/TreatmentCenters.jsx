import React, { useState } from "react";
import "./TreatmentCenters.css";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import axios from "axios";
import slugify from "react-slugify";
import Button from "react-bootstrap/Button";
import apiLink from "../../api.config";

// used to cancel operations when there is a new request
let cancelToken;

// gets the number of treatment centers with the selected filters
async function fetchTreatmentCenterCount(filter, selectedOption) {
  let count = 0;
  let baseQuery = `/centers/count?`;

  if (cancelToken) {
    cancelToken.cancel("Operation canceled due to new request.");
  }
  cancelToken = axios.CancelToken.source();

  // applies selected filters
  if (filter === "ownership") {
    baseQuery += "ownership=" + selectedOption;
  } else if (filter === "payment") {
    baseQuery += "payment=" + selectedOption;
  } else if (filter === "category") {
    baseQuery += "category=" + selectedOption;
  } else if (filter === "testing") {
    baseQuery += "testing=" + selectedOption;
  } else if (filter === "search" && selectedOption !== "") {
    baseQuery += `query=${selectedOption}`;
  }

  await axios.get(apiLink + baseQuery, { cancelToken: cancelToken.token }).then((response) => {
    count = response.data.count;
  }).catch((error) => {
    return 0;
  });
  return count;
}

// Fetch the treatment centers from the API  with the selected filters
async function fetchTreatmentCenters(filter, selectedOption, pageSize, page) {
  let centers = [];
  let baseQuery = `/centers?page=${page ?? 1}&pageSize=${pageSize ?? 20}`;

  // applies selected filters
  if (filter === "ownership") {
    baseQuery += "&ownership=" + selectedOption;
  } else if (filter === "payment") {
    baseQuery += "&payment=" + selectedOption;
  } else if (filter === "distance") {
    baseQuery += `&latitude=${selectedOption.latitude}&longitude=${selectedOption.longitude}`;
  } else if (filter === "category") {
    baseQuery += "&category=" + selectedOption;
  } else if (filter === "testing") {
    baseQuery += "&testing=" + selectedOption;
  } else if (filter === "search" && selectedOption !== "") {
    baseQuery += `&query=${selectedOption}`;
  }

  await axios
    .get(apiLink + baseQuery)
    .then((response) => {
      centers = response.data;
    })
    .catch((error) => {
      return [];
    });
  return centers;
}

export default function TreatmentCenters() {
  const pageSize = 20;
  const [page, setPage] = React.useState(1);
  const [treatmentCenters, setTreatmentCenters] = React.useState([]);
  const [totalCenterCount, setTotalCenterCount] = React.useState(0);
  const [searchQuery, setSearchQuery] = React.useState("");
  const [selectedFilter, setSelectedFilter] = useState("");
  const [selectedOption, setSelectedOption] = useState("");
  const [geolocation, setGeolocation] = useState({ latitude: 0, longitude: 0 });

  // highlights the search term in the results
  const SearchResult = ({ text, searchTerm }) => {
    return <div>{highlightSearchTermCenters(text, searchTerm)}</div>;
  };

  // Fetches the cities from the API on page load, using given long/lat coords
  React.useEffect(() => {
    setTreatmentCenters([]);
    fetchTreatmentCenterCount(selectedFilter, selectedOption).then((count) => {
      setTotalCenterCount(count);
    });

    let longitude = null;
    let latitude = null;
    navigator.geolocation.getCurrentPosition((position) => {
      longitude = position.coords.longitude;
      latitude = position.coords.latitude;
      setGeolocation({ longitude, latitude });
    });

    fetchTreatmentCenters(null, pageSize, page).then((centers) => {
      setTreatmentCenters(centers);
    });
  }, []);

  // if the filter changes, then update the treatment centers displayed
  React.useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      fetchTreatmentCenters(
        selectedFilter,
        selectedOption,
        pageSize,
        page
      ).then((centers) => {
        setTreatmentCenters(centers);
      });
    }, 300);
    return () => clearTimeout(delayDebounceFn);
  }, [page, selectedOption]);

  // if the filter changes, then update the count displayed
  React.useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      fetchTreatmentCenterCount(selectedFilter, selectedOption).then(
        (count) => {
          setTotalCenterCount(count);
        }
      );
    }, 300);
    return () => clearTimeout(delayDebounceFn);
  }, [selectedOption]);

  // update search query when the user searches something
  const handleSearchChange = (e) => {
    setSelectedFilter("search");
    setSelectedOption(e.target.value);
    setSearchQuery(e.target.value);
  };

  // displays the number of instances on this page based on instances in total
  function pageCounter(page, pageSize, totalCenterCount) {
    if (totalCenterCount == 0) {
      return "Displaying results 0 - 0."
    }
    return ("Displaying results " + ((page - 1) * pageSize + 1) + " - " + (Math.min((page * pageSize - 1), totalCenterCount) + " out of " + totalCenterCount + " instances."))
  }

  return (
    <div className="centers-container">
      <div className="sub-header">
        <h1>Treatment Centers</h1>
        <p>
          There are several treatment centers across the country that can help
          rehabilitate those with substance abuse disorders. Click on a card to
          learn more.
        </p>
        <input
          type="text"
          placeholder="Search..."
          defaultValue={searchQuery}
          onChange={handleSearchChange}
          className="search-bar rounded-pill"
        />
      </div>
      <div className="sub-content-container">
        <div className="sub-sort-container">
          <p>
            {pageCounter(page, pageSize, totalCenterCount)}
          </p>
          <p>Page {page} of results.</p>
          <FilterMenu
            selectedFilter={selectedFilter}
            setSelectedFilter={setSelectedFilter}
            setSelectedOption={setSelectedOption}
            geolocation={geolocation}
            setGeolocation={setGeolocation}
          />
          <div className="next-button-container">
            <div className="top-buttons">
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(page - 1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                Previous
              </Button>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(page + 1)}
                disabled={page * pageSize >= totalCenterCount}
                className="substance-pagination"
              >
                Next
              </Button>
            </div>
            <div className="bottom-buttons" style={{ marginLeft: "1.5rem" }}>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                First
              </Button>
              <Button
                style={{
                  backgroundColor: "#536EAD",
                  color: "#fff",
                  borderStyle: "none",
                  margin: "0.5rem",
                }}
                onClick={() => setPage(Math.ceil(totalCenterCount / pageSize))}
                disabled={page * pageSize >= totalCenterCount}
                className="substance-pagination"
              >
                Last
              </Button>
            </div>
          </div>
        </div>
        <div className="sub-card-container">
          {treatmentCenters.length !== 0
            ? (treatmentCenters.map((center, index) => {
              return (
                <Card key={index} className="card">
                  <Link
                    to={`/centers/${slugify(center.name1 + center.name2)}`}
                    state={{ center: center }}
                    className="link"
                  >
                    <Card.Body>
                      <Card.Title>
                        <SearchResult
                          text={center.name1}
                          searchTerm={searchQuery}
                        />
                      </Card.Title>
                      <Card.Subtitle>
                        <SearchResult
                          text={center.name2}
                          searchTerm={searchQuery}
                        />
                      </Card.Subtitle>
                      <Card.Text>
                        <SearchResult
                          text={`${center.street1}, ${center.city}, ${center.state}, ${center.zip}`}
                          searchTerm={searchQuery}
                        />
                        <br />
                        <SearchResult
                          text={center.phone}
                          searchTerm={searchQuery}
                        />
                        <br />
                        <SearchResult
                          text={center.email}
                          searchTerm={searchQuery}
                        />
                      </Card.Text>
                    </Card.Body>
                  </Link>
                </Card>
              );
            })
            ) : (
              <div>No Centers Available.</div>
            )}
        </div>
      </div>
    </div>
  );
}

// Highlights the search term using the specified CSS
const highlightSearchTermCenters = (text, searchTerm) => {
  if (!text) return null;
  if (searchTerm.length > 0) {
    const regex = new RegExp(`(${searchTerm})`, "gi");
    const parts = text.split(regex);
    return parts.map((part, index) =>
      regex.test(part) ? (
        <span key={index} className="highlight">
          {part}
        </span>
      ) : (
        part
      )
    );
  }
  // no search term entered, do nothing
  else {
    return text;
  }
};

// displays information for filtering
const FilterMenu = ({
  onFilterChange,
  selectedFilter,
  setSelectedFilter,
  setSelectedOption,
  geolocation,
  setGeolocation,
}) => {
  const handleFilterChange = (e) => {
    setSelectedFilter(e.target.value);
    if (onFilterChange) {
      onFilterChange(e.target.value);
    }
  };

  // sets up options to specify more information when certain filter is selected
  const [selectedButton, setSelectedButton] = useState(""); // State to keep track of the selected private/public option
  const [selectedPaymentOption, setSelectedPaymentOption] = useState(""); // State to keep track of the selected payment option
  const [selectedSubstanceType, setSelectedSubstanceType] = useState(""); // State to keep track of the selected substance type
  const [selectedSubstance, setSelectedSubstance] = useState(""); // State to keep track of the selected substance

  const handleButtonSelect = (value) => {
    setSelectedButton(value);
    setSelectedOption(value);
  };
  const handlePaymentOptionSelect = (value) => {
    setSelectedPaymentOption(value);
    setSelectedOption(value);
  };
  const handleSubstanceTypeSelect = (value) => {
    setSelectedSubstanceType(value);
    setSelectedOption(value);
  };
  const handleSubstanceSelect = (value) => {
    setSelectedSubstance(value);
    setSelectedOption(value);
  };

  let ownershipButtons = null;
  let paymentButtons = null;
  let distanceButtons = null;
  let categoryButtons = null;
  let testingButtons = null;
  if (selectedFilter == "ownership") {
    ownershipButtons = (
      <div style={{ marginTop: "10px" }}>
        <Button
          onClick={() => handleButtonSelect("Public")}
          style={{
            marginRight: "10px",
            backgroundColor: selectedButton === "Public" ? "#536EAD" : "#ccc",
            color: selectedButton === "Public" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Public
        </Button>
        <Button
          onClick={() => handleButtonSelect("Private")}
          style={{
            backgroundColor: selectedButton === "Private" ? "#536EAD" : "#ccc",
            color: selectedButton === "Private" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Private
        </Button>
      </div>
    );
  }
  if (selectedFilter == "payment") {
    paymentButtons = (
      <div style={{ marginTop: "10px" }}>
        <Button
          onClick={() => handlePaymentOptionSelect("Medicare")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedPaymentOption === "Medicare" ? "#536EAD" : "#ccc",
            color: selectedPaymentOption === "Medicare" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Medicare
        </Button>
        <Button
          onClick={() => handlePaymentOptionSelect("Medicaid")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedPaymentOption === "Medicaid" ? "#536EAD" : "#ccc",
            color: selectedPaymentOption === "Medicaid" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Medicaid
        </Button>
        <Button
          onClick={() => handlePaymentOptionSelect("Military Insurance")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedPaymentOption === "Military Insurance"
                ? "#536EAD"
                : "#ccc",
            color:
              selectedPaymentOption === "Military Insurance" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Military Insurance
        </Button>
        <Button
          onClick={() => handlePaymentOptionSelect("No Payment Accepted")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedPaymentOption === "No Payment Accepted"
                ? "#536EAD"
                : "#ccc",
            color:
              selectedPaymentOption === "No Payment Accepted" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          No Payment Accepted
        </Button>
        <Button
          onClick={() => handlePaymentOptionSelect("Private Health Insurance")}
          style={{
            backgroundColor:
              selectedPaymentOption === "Private Health Insurance"
                ? "#536EAD"
                : "#ccc",
            color:
              selectedPaymentOption === "Private Health Insurance"
                ? "#fff"
                : "#000",
            borderStyle: "none",
          }}
        >
          Private Health Insurance
        </Button>
      </div>
    );
  }
  if (selectedFilter == "distance") {
    distanceButtons = (
      <div>
        Longitude:
        <input
          placeholder="0"
          value={geolocation.longitude}
          type="number"
          name="longitude"
          onChange={(e) => {
            setGeolocation({
              longitude: e.target.value,
              latitude: geolocation.latitude,
            });

            setSelectedOption({
              longitude: e.target.value,
              latitude: geolocation.latitude,
            });
          }}
        />
        Latitude:
        <input
          placeholder="0"
          value={geolocation.latitude}
          type="number"
          name="latitude"
          onChange={(e) => {
            setGeolocation({
              latitude: e.target.value,
              longitude: geolocation.longitude,
            });

            setSelectedOption({
              longitude: geolocation.longitude,
              latitude: e.target.value,
            });
          }}
        />
        <Button
          onClick={() => {
            navigator.geolocation.getCurrentPosition((position) => {
              setGeolocation({
                longitude: position.coords.longitude,
                latitude: position.coords.latitude,
              });

              setSelectedOption({
                longitude: position.coords.longitude,
                latitude: position.coords.latitude,
              });
            });
          }}
        >
          Reset Location
        </Button>
      </div>
    );
  }
  if (selectedFilter == "category") {
    categoryButtons = (
      <div style={{ marginTop: "10px" }}>
        <Button
          onClick={() => handleSubstanceTypeSelect("Stimulant")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedSubstanceType === "Stimulant" ? "#536EAD" : "#ccc",
            color: selectedSubstanceType === "Stimulant" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Stimulant
        </Button>
        <Button
          onClick={() => handleSubstanceTypeSelect("Depressant")}
          style={{
            backgroundColor:
              selectedSubstanceType === "Depressant" ? "#536EAD" : "#ccc",
            color: selectedSubstanceType === "Depressant" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Depressant
        </Button>
      </div>
    );
  }
  if (selectedFilter == "testing") {
    testingButtons = (
      <div style={{ marginTop: "10px" }}>
        <Button
          onClick={() => handleSubstanceSelect("Alcohol")}
          style={{
            marginRight: "10px",
            backgroundColor:
              selectedSubstance === "Alcohol" ? "#536EAD" : "#ccc",
            color: selectedSubstance === "Alcohol" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Alcohol
        </Button>
        <Button
          onClick={() => handleSubstanceSelect("Drug")}
          style={{
            backgroundColor: selectedSubstance === "Drug" ? "#536EAD" : "#ccc",
            color: selectedSubstance === "Drug" ? "#fff" : "#000",
            borderStyle: "none",
          }}
        >
          Drug
        </Button>
      </div>
    );
  }

  return (
    <div className="filter-card-container" style={{ marginBottom: "20px" }}>
      <label style={{ marginRight: "10px" }} htmlFor="filterOptions">
        Filter By:
      </label>
      <select
        id="filterOptions"
        value={selectedFilter}
        onChange={handleFilterChange}
        style={{
          padding: "8px 12px",
          fontSize: "16px",
          border: "1px solid #ccc",
          borderRadius: "4px",
        }}
      >
        <option value="">Select an option</option>
        <option value="ownership">Ownership</option>
        <option value="payment">Payment</option>
        <option value="distance">Distance</option>
        <option value="category">Category</option>
        <option value="testing">Testing</option>
      </select>

      {ownershipButtons}
      {paymentButtons}
      {distanceButtons}
      {categoryButtons}
      {testingButtons}
    </div>
  );
};
