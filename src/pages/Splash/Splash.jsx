import React, { useEffect } from 'react'
import { Link, useNavigate } from 'react-router-dom';
import { useState } from 'react';
import './Splash.css'


export default function Splash() {
  // FAQ data array
  const faqs = [
    {
      question: 'What are the signs and symptoms of substance abuse?',
      answer: 'Common signs and symptoms of substance abuse include changes in behavior, mood swings, withdrawal from friends and family, neglecting responsibilities, and physical changes such as weight loss or gain.'
    },
    {
      question: 'What treatment options are available for substance abuse?',
      answer: 'Treatment options for substance abuse include therapy, counseling, support groups, medication-assisted treatment, and residential rehabilitation programs. For more information, visit treatment centers.'
    },
    {
      question: 'Who can I contact for help if I am suffering from drug addiction?',
      answer: 'Please call the Substance Abuse and Mental Health Services Administration at 1-800-662-4357.'
    },
    {
      question: 'How can I support a loved one struggling with substance abuse?',
      answer: 'Supporting a loved one struggling with substance abuse involves open communication, offering emotional support, encouraging treatment, and setting boundaries to protect yourself.'
    },
    {
      question: 'Are there specific risk factors for developing substance abuse disorders?',
      answer: 'Yes, some common risk factors for developing substance abuse disorders include genetics, environment, trauma, mental health disorders, and peer pressure.'
    },
    {
      question: 'What should I do if I suspect someone is abusing drugs or alcohol?',
      answer: 'If you suspect someone is abusing drugs or alcohol, express your concerns in a caring and non-judgmental manner, offer support, and encourage them to seek professional help - for more information, visit treatment centers.'
    }
  ];

  // Testimonials data array
  const testimonials = [
    {
      name: 'John Doe',
      city: 'New York',
      state: 'NY',
      testimonial: 'I struggled with substance abuse for years, but with the help of therapy, support groups, and my loved ones, I was able to overcome my addiction. Today, I am living a healthy, sober life and am grateful for every moment.',
    },
    {
      name: 'Jane Smith',
      city: 'Los Angeles',
      state: 'CA',
      testimonial: 'As someone who has battled substance abuse, I understand the challenges it brings. However, with determination and the right support system, recovery is possible. I encourage anyone struggling to seek help and know that they are not alone in their journey.',
    },
  ];

  // State to manage the visibility of each answer
  const [visibleAnswers, setVisibleAnswers] = useState(Array(faqs.length).fill(false));
  const navigate = useNavigate();

  // Function to toggle the visibility of an answer
  const toggleAnswer = (index) => {
    const newVisibleAnswers = [...visibleAnswers];
    newVisibleAnswers[index] = !newVisibleAnswers[index];
    setVisibleAnswers(newVisibleAnswers);

    const faqItems = document.querySelectorAll('.faq-item');
    faqItems[index].classList.toggle('active');
  };

  const [searchQuery, setSearchQuery] = useState('');

  // Performs the global search
  useEffect(() => {
    const delayDebounceFn = setTimeout(() => {
      if (searchQuery === '') return;
      navigate(`/search?query=${searchQuery}`);
    }, 2000);

    return () => clearTimeout(delayDebounceFn);
  }, [searchQuery]);

  // Updates the search query when a user types something in
  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  return (
    <div>
      <section className="home display-flex">
        <div className="grid-container display-flex flex-column content">
          <div className="grid-100 mobile-grid-100 tablet-grid-100 bubble-dark text-align-center">
            <h1>
              A Comprehensive Look at Substance Abuse Across the United States.
            </h1>
          </div>
          <div className="mt-3 bubble-light">
            <Link to="/substances" className="btn btn-primary rounded-pill button-style"> Substances </Link>
            <Link to="/centers" className="btn btn-primary rounded-pill ms-3 button-style"> Treatment Centers </Link>
            <Link to="/cities" className="btn btn-primary rounded-pill ms-3 button-style"> City Statistics </Link>
            <Link to="/about" className="btn btn-primary rounded-pill ms-3 button-style"> About </Link>
            <Link to="/visualizations" className="btn btn-primary rounded-pill ms-3 button-style"> Our Visualizations </Link>
            <Link to="/dev-visualizations" className="btn btn-primary rounded-pill ms-3 button-style"> Developer Visualizations </Link>
          </div>
          <input
            type="text"
            placeholder="Search..."
            value={searchQuery}
            onChange={handleSearchChange}
            className="search-bar rounded-pill"
          />
        </div>
      </section>

      <section className="testimonials-section">
        <h2 className="text-center">Testimonials</h2>
        <div className="testimonials-grid">
          {testimonials.map((testimonial, index) => (
            <div key={index} className="testimonial-card">
              <h3>{testimonial.name}, {testimonial.city}, {testimonial.state}</h3>
              <p>{testimonial.testimonial}</p>
            </div>
          ))}
        </div>
      </section>

      <section className="faq-section">
        <h2 className="text-center">Frequently Asked Questions</h2>
        <div className="faq-grid">
          {faqs.map((faq, index) => (
            <div key={index} className="faq-item" onClick={() => toggleAnswer(index)}>
              <h3>{faq.question}</h3>
              {visibleAnswers[index] && (
                <p>
                  {faq.answer.split(' ').map((word, i) => (
                    word === 'centers.' ? <a href="/centers" key={i}>{word}</a> : word + ' '
                  ))}
                </p>
              )}
            </div>
          ))}
        </div>
      </section>
    </div>
  );
}
