import React, { useState } from 'react';
import "./Substances.css";
import Card from "react-bootstrap/Card";
import { Link } from "react-router-dom";
import axios from "axios";
import slugify from "react-slugify";
import Button from "react-bootstrap/Button";
import apiLink from "../../api.config";

// Fetch the substances count from the API based on the search term/filter 
async function fetchSubstancesCount(selectedFilter, searchQuery) {
  let count = 0;
  if (searchQuery) {
    await axios.get(apiLink + `/substances/count?query=${searchQuery}`).then((response) => {
      count = response.data.count;
    })
  }
  else if (selectedFilter == "Sedative" || selectedFilter == "Stimulant" || selectedFilter == "Depressant" || selectedFilter == "Other") {
    await axios.get(apiLink + `/substances/count?category=${selectedFilter}`).then((response) => {
      count = response.data.count;
    })
  } else {
    await axios.get(apiLink + "/substances/count").then((response) => {
      count = response.data.count;
    })
  }
  return count;
}

// Fetch the substances from the API based on the search term/filter 
async function fetchSubstances(pageSize, page, selectedFilter, searchQuery) {
  let substances = [];
  if (searchQuery) {
    await axios.get(apiLink + `/substances?page=${page}&pageSize=${pageSize}&query=${searchQuery}`).then((response) => {
      substances = response.data;
    })
  }
  else if (selectedFilter == "A-Z" || selectedFilter == "Z-A") {
    await axios.get(apiLink + `/substances?page=${page}&pageSize=${pageSize}&sort=${selectedFilter}`).then((response) => {
      substances = response.data;
    })
  }
  else if (selectedFilter == "Sedative" || selectedFilter == "Stimulant" || selectedFilter == "Depressant" || selectedFilter == "Other") {
    await axios.get(apiLink + `/substances?page=${page}&pageSize=${pageSize}&category=${selectedFilter}`).then((response) => {
      substances = response.data;
    })
  }
  else {
    await axios.get(apiLink + `/substances?page=${page}&pageSize=${pageSize}`).then((response) => {
      substances = response.data;
    })
  }
  return substances;
}

export default function Substances() {
  const pageSize = 20;
  const [page, setPage] = React.useState(1);
  const [substances, setSubstances] = React.useState([]);
  const [totalSubstancesCount, setTotalSubstancesCount] = React.useState(0);
  const [searchQuery, setSearchQuery] = React.useState('');
  const [selectedFilter, setSelectedFilter] = useState('');

  // highlights the search term in the results
  const SearchResult = ({ text, searchTerm }) => {
    return (
      <div>
        {highlightSearchTerm(text, searchTerm)}
      </div>
    );
  };

  // Fetches the substances/count from the API on page load
  React.useEffect(() => {
    setSubstances([]);
    fetchSubstancesCount(selectedFilter, searchQuery).then((count) => {
      setTotalSubstancesCount(count);
    });

    fetchSubstances(pageSize, page, selectedFilter, searchQuery).then((substances) => {
      setSubstances(substances);
    });
  }, []);

  // updates page on change
  React.useEffect(() => {
    const fetchData = async () => {
      const substances = await fetchSubstances(pageSize, page, selectedFilter, searchQuery);
      setSubstances(substances);
      const count = await fetchSubstancesCount(selectedFilter, searchQuery);
      setTotalSubstancesCount(count);
      setPage(1);
    };

    fetchData();
  }, [pageSize, page, selectedFilter, searchQuery]);

  // updates variable to store selected search term
  const handleSearchChange = (e) => {
    setSearchQuery(e.target.value);
  };

  // updates variable to store selected filters
  const handleFilterChange = (e) => {
    setSelectedFilter(e.target.value);
  };

  // updates page counter based on the current page
  function pageCounter(page, pageSize, totalSubstancesCount) {
    if (totalSubstancesCount == 0) {
      return "Displaying results 0 - 0."
    }
    return ("Displaying results " + ((page - 1) * pageSize + 1) + " - " + (Math.min((page * pageSize - 1), totalSubstancesCount) + " out of " + totalSubstancesCount + " instances."))
  }

  return (
    <div className="substances-container">
      <div className="sub-header">
        <h1>Substances</h1>
        <p>There are a wide variety of substances that must be handled responsibly. They can be highly addictive, and therefore, can have dangerous side effects. Click on a card to learn more.</p>
        <input
          type="text"
          placeholder="Search..."
          value={searchQuery}
          onChange={handleSearchChange}
          className="search-bar rounded-pill"
        />
      </div>

      <div className="sub-content-container">
        <div className="sub-sort-container">
          <p>
            {pageCounter(page, pageSize, totalSubstancesCount)}
          </p>
          <p>
            Page {page} of results.
          </p>
          <FilterMenu
            onFilterChange={handleFilterChange}
            selectedFilter={selectedFilter}
            setSelectedFilter={setSelectedFilter}
          />
          <div className="next-button-container"  >
            <div className="top-buttons" >
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                onClick={() => setPage(page - 1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                Previous
              </Button>
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                onClick={() => setPage(page + 1)}
                disabled={page * pageSize >= totalSubstancesCount}
                className="substance-pagination"
              >
                Next
              </Button>
            </div>
            <div className="bottom-buttons" style={{ marginLeft: '1.5rem' }}>
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                onClick={() => setPage(1)}
                disabled={page === 1}
                className="substance-pagination"
              >
                First
              </Button>
              <Button
                style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                onClick={() => setPage(Math.ceil(totalSubstancesCount / pageSize))}
                disabled={page * pageSize >= totalSubstancesCount}
                className="substance-pagination"
              >
                Last
              </Button>

            </div>

          </div>
        </div>
        <div className="sub-card-container">
          {substances.length !== 0
            ? (substances.map((substance, index) => {
              return (
                <Card key={index} className="card">
                  <Link
                    to={`/substances/${slugify(substance.name)}`}
                    state={{ substance: substance }}
                    className="link"
                  >
                    <Card.Body>
                      <Card.Title>
                        <SearchResult text={substance.name} searchTerm={searchQuery} />
                      </Card.Title>
                      <Card.Subtitle>
                        <SearchResult text={substance.category} searchTerm={searchQuery} />
                      </Card.Subtitle>
                      <Card.Text>
                        <br />
                        <SearchResult text={substance.description.substring(0, 200) + "..."} searchTerm={searchQuery} />
                      </Card.Text>
                    </Card.Body>
                  </Link>
                </Card>
              );
            })
            ) : (
              <div>No Substances Available.</div>

            )}
        </div>
      </div>
    </div>
  );
}

// Highlights the search term using the specified CSS
const highlightSearchTerm = (text, searchTerm) => {
  if (searchTerm.length > 0) {
    const regex = new RegExp(`(${searchTerm})`, 'gi');
    const parts = text.split(regex);

    return parts.map((part, index) =>
      regex.test(part) ? <span key={index} className="highlight">{part}</span> : part
    );
  }
  // no search term entered, do nothing
  else {
    return text;
  }

};

// displays information for filtering
const FilterMenu = ({ onFilterChange, selectedFilter, setSelectedFilter }) => {
  const handleFilterChange = (e) => {
    setSelectedFilter(e.target.value);
    if (onFilterChange) {
      onFilterChange(e.target.value);
    }
  };

  return (
    <div className="filter-card-container" style={{ marginBottom: '20px' }}>
      <label style={{ marginRight: '10px' }} htmlFor="filterOptions">
        Filter By:
      </label>
      <select
        id="filterOptions"
        value={selectedFilter}
        onChange={handleFilterChange}
        style={{
          padding: '8px 12px',
          fontSize: '16px',
          border: '1px solid #ccc',
          borderRadius: '4px',
        }}
      >
        <option value="none">Select an option</option>
        <option value="A-Z">Alphabetical: A-Z</option>
        <option value="Sedative">Category: Sedatives</option>
        <option value="Stimulant">Category: Stimulants</option>
        <option value="Depressant">Category: Depressants</option>
        <option value="Other">Category: Other</option>
      </select>
    </div>
  );
}