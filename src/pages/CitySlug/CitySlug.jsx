import React, { useState, useEffect } from 'react';
import { useLocation } from 'react-router-dom';
import { Link, Route } from 'react-router-dom';
import slugify from 'react-slugify'
import Row from "react-bootstrap/Row";
import Card from "react-bootstrap/Card";
import axios from "axios";
import apiLink from "../../api.config";
import Button from "react-bootstrap/Button";
import './CitySlug.css'

// Get linked centers in this city
async function fetchLinkedCenters(pageSize, page, cityname) {
  let centers = [];
  await axios.get(apiLink + `/cities/getCenters?page=${page}&pageSize=${pageSize}&city=${cityname}`).then((response) => {
    centers = response.data;
  })
  return centers;
}

// Get linked drugs used in this city
async function fetchLinkedDrugs(pageSize, page, category) {
  let drugs = [];
  await axios.get(apiLink + `/substances?page=${page}&pageSize=${pageSize}&category=${category}`).then((response) => {
    drugs = response.data;
  })
  return drugs;
}

// fetch the city image
const getCityImage = async (cityName) => {
  const searchEngineID = "3704f038e1c3a4284";
  const key = "AIzaSyA9W82TLurnDqqGbFNwtOO5JB_qlxv7VZ4";
  const url = `https://www.googleapis.com/customsearch/v1?key=${key}&cx=${searchEngineID}&q=${cityName}&searchType=image`;
  const response = await fetch(url);
  const data = await response.json();

  if (data && data.items && data.items.length > 0) {
    const imageURL = data.items[0].link;
    return imageURL;
  } else {
    return null;
  }
};

export default function CitySlug(props) {
  const pageSize = 20;
  const [page, setPage] = React.useState(1);
  const { state } = useLocation();
  const { city } = state;
  const [cityImage, setCityImage] = useState(null);
  const [drugs, setDrugs] = React.useState([]);
  const [centers, setCenters] = React.useState([]);
  const [leadingcategory, setLeadingCategory] = React.useState(state.city.leadingcategory);
  const [imgURL, setimgurl] = React.useState(state.city.imageurl);
  const [cityname, setCityName] = React.useState(city.name.split(", ")[0]);
  let mapUrl = `https://maps.google.com/maps?q=${city.lat},${city.long}&z=15&output=embed`;

  useEffect(() => {
    // Fetch the city image when the component mounts
    const fetchCityImage = async () => {
      const imageURL = await getCityImage(city.name);
      setCityImage(imageURL);
    };
    fetchCityImage();
  }, [city.name]);

  // Fetch the drugs and centers from the API on page load
  React.useEffect(() => {
    setDrugs([]);
    setCenters([])
    fetchLinkedDrugs(20, page, leadingcategory).then((drugs) => {
      setDrugs(drugs);
    });
    fetchLinkedCenters(20, page, cityname).then((centers) => {
      setCenters(centers);
    });
  }, []);

  // Fetch the treatment centers from the API as page changes 
  React.useEffect(() => {
    fetchLinkedCenters(20, page, cityname).then((centers) => {
      setCenters(centers);
    });
  }, [page]);

  // Fetch the drugs from the API as page changes 
  React.useEffect(() => {
    fetchLinkedDrugs(20, page, leadingcategory).then((drugs) => {
      setDrugs(drugs);
    });
  }, [page]);

  const totalCenterCount = centers.length;
  const totalSubstancesCount = drugs.length;

  return (
    <div className="center-slug-container">
      <div className="top-container">
        <div className="left-container">
          <div className="header-container">
            <div className="name-container">
              <h1>{city.name} <br /></h1>
            </div>
            <div className="drugstats-container">
              <h4 style={{ textDecoration: 'none', color: '#808080' }}>Drug Use Stats: </h4>
              <h5>Marijuana Use Rate: {city.marijuana}</h5>
              <h5>Cocaine Use Rate: {city.cocaine}</h5>
              <h5>Heroin Use Rate: {city.heroin}</h5>
              <h5>Meth Use Rate: {city.meth}</h5>
            </div>
            <div className="centers-container">
              <h5>Region in the U.S.: {city.region}</h5>
            </div>
          </div>
          <div className="media-container">
            <div className="map-container">
              <iframe src={mapUrl} />
            </div>
            <div className="img-container">
              <img src={imgURL} />
            </div>
          </div>
        </div>
      </div>
      <div className="bottom-container">
        <div className="city-related-centers">
          <div className="bottom-header">
            <h3><b>Related Centers</b></h3>
          </div>
          <div className="city-centers-rows">
            <div className="pagination-info-cities">
              <p>
                Displaying results {+(page - 1) * pageSize + 1} -{" "}
                {+(Math.min((page) * pageSize, totalCenterCount))} out
                of {+totalCenterCount} total instances.
              </p>
              <p>
                Page {page} of results.
              </p>
              <div className="next-button-container"  >
                <div className="top-buttons" >
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(page - 1)}
                    disabled={page === 1}
                    className="substance-pagination"
                  >
                    Previous
                  </Button>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(page + 1)}
                    disabled={page * pageSize >= totalCenterCount}
                    className="substance-pagination"
                  >
                    Next
                  </Button>
                </div>
                <div className="bottom-buttons" style={{ marginLeft: '1.5rem' }}>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(1)}
                    disabled={page === 1}
                    className="substance-pagination"
                  >
                    First
                  </Button>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(Math.ceil(totalCenterCount / pageSize))}
                    disabled={page * pageSize >= totalCenterCount}
                    className="substance-pagination"
                  >
                    Last
                  </Button>
                </div>
              </div>
            </div>
            <div className="bottom-content">
              <Row className="justify-content-center">
                {centers.map((center, index) => (
                  <Card className="card" key={index}>
                    <Link to={`/centers/${slugify(center.name1 + center.name2)}`}
                      state={{ center: center }}
                      className="instance-link" style={{ textDecoration: 'none', color: '#000' }}>
                      <Card.Body>
                        <Card.Title>{center.name1}</Card.Title>
                        <Card.Text>
                          Address: {center.street1}, {center.city}, {center.state}, {center.zip}<br />
                          {center.phone}<br />
                          {center.email}
                        </Card.Text>
                      </Card.Body>
                    </Link>
                  </Card>
                ))}
              </Row>
            </div>
          </div>
        </div>
        <div className="city-related-sub">
          <div className="bottom-header">
            <h3><b>Related Drugs</b></h3>
          </div>
          <div className="city-sub-rows">
            <div className="pagination-info-cities">
              <p>
                Displaying results {+((page - 1) * pageSize) + 1} -{" "}
                {+(Math.min((page) * pageSize, totalSubstancesCount))} out
                of {+(totalSubstancesCount)} total instances
              </p>
              <p>
                Page {page} of results.
              </p>
              <div className="next-button-container"  >
                <div className="top-buttons" >
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(page - 1)}
                    disabled={page === 1}
                    className="substance-pagination"
                  >
                    Previous
                  </Button>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(page + 1)}
                    disabled={page * pageSize >= totalSubstancesCount}
                    className="substance-pagination"
                  >
                    Next
                  </Button>
                </div>
                <div className="bottom-buttons" style={{ marginLeft: '1.5rem' }}>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(1)}
                    disabled={page === 1}
                    className="substance-pagination"
                  >
                    First
                  </Button>
                  <Button
                    style={{ backgroundColor: '#536EAD', color: '#fff', borderStyle: 'none', margin: '0.5rem' }}
                    onClick={() => setPage(Math.ceil(totalSubstancesCount / pageSize))}
                    disabled={page * pageSize >= totalSubstancesCount}
                    className="substance-pagination"
                  >
                    Last
                  </Button>
                </div>
              </div>
            </div>
            <div className="bottom-content">
              <Row className="justify-content-center">
                {drugs.map((drug, index) => (
                  <Card className="card" key={index}>
                    <Link to={`/substances/${slugify(drug.name)}`}
                      state={{ substance: drug }} className="instance-link" style={{ textDecoration: 'none', color: '#000' }}>
                      <Card.Body>
                        <Card.Title>{drug.name}</Card.Title>
                        <Card.Subtitle>{drug.category}</Card.Subtitle>
                        <Card.Text>
                          <br />
                          {drug.description.substring(0, 200) + "..."}
                        </Card.Text>
                      </Card.Body>
                    </Link>
                  </Card>
                ))}
              </Row>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
