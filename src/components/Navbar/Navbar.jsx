import React from 'react';
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import './Navbar.css'

export default function NavbarComponent() {
  return (
    <Navbar expand="lg" className="nav-container">
      <Navbar.Brand href="/">
        <img className="nav-brand nav-logo"
          src="/SubstanceAbuseAwarenessLogo.png"
          alt="Logo"
        />
      </Navbar.Brand>
      <Navbar.Toggle aria-controls="basic-navbar-nav" />
      <Navbar.Collapse id="basic-navbar-nav">
        <Nav className="me-auto nav">
          <Nav.Link href="/substances">Substances</Nav.Link>
          <Nav.Link href="/centers">Treatment Centers</Nav.Link>
          <Nav.Link href="/cities">City Statistics</Nav.Link>
          <Nav.Link href="/about">About</Nav.Link>
          <Nav.Link href="/visualizations">Our Visualizations</Nav.Link>
          <Nav.Link href="/dev-visualizations">Developer's Visualizations</Nav.Link>
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  );
}