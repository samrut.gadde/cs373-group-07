import React from 'react';
import { render, screen} from '@testing-library/react';
import {MemoryRouter } from 'react-router-dom'; 
import NavbarComponent from '../src/components/Navbar/Navbar.jsx';
import Splash from '../src/pages/Splash/Splash.jsx';
import Substances from '../src/pages/Substances/Substances.jsx';
import TreatmentCenters from '../src/pages/TreatmentCenters/TreatmentCenters.jsx';
import Cities from '../src/pages/Cities/Cities.jsx';
import '@testing-library/jest-dom';

// fake coordinates to test with
const mockGeolocation = {
  getCurrentPosition: jest.fn()
    .mockImplementationOnce((success) => Promise.resolve(success({
      coords: {
        latitude: 51.1,
        longitude: 45.3
      }
    })))
};
global.navigator.geolocation = mockGeolocation;

describe('NavbarComponent', () => {
  test('renders navbar with logo and links', () => {
    render(<NavbarComponent />);
    
    // Check if logo is rendered
    const logoElement = screen.getByAltText('Logo');
    expect(logoElement).toBeInTheDocument();

    // Check if navbar links are rendered
    const substancesLink = screen.getByText('Substances');
    expect(substancesLink).toBeInTheDocument();

    const treatmentCentersLink = screen.getByText('Treatment Centers');
    expect(treatmentCentersLink).toBeInTheDocument();

    const cityStatisticsLink = screen.getByText('City Statistics');
    expect(cityStatisticsLink).toBeInTheDocument();

    const aboutLink = screen.getByText('About');
    expect(aboutLink).toBeInTheDocument();
  });
});

describe('Splash Page', () => {
    test('renders splash correctly', () => {
      render(
        <MemoryRouter>
          <Splash />
        </MemoryRouter>
      );
  
      // Check if the main heading is rendered
      const mainHeading = screen.getByText('A Comprehensive Look at Substance Abuse Across the United States.');
      expect(mainHeading).toBeInTheDocument();
  
      // Check if the navigation links are rendered
      const substancesLink = screen.getByRole('link', { name: 'Substances' });
      expect(substancesLink).toBeInTheDocument();
  
      const treatmentCentersLink = screen.getByRole('link', { name: 'Treatment Centers' });
      expect(treatmentCentersLink).toBeInTheDocument();
  
      const cityStatisticsLink = screen.getByRole('link', { name: 'City Statistics' });
      expect(cityStatisticsLink).toBeInTheDocument();
  
      const aboutLink = screen.getByRole('link', { name: 'About' });
      expect(aboutLink).toBeInTheDocument();
  
      // Check if the testimonials section is rendered
      const testimonialsHeading = screen.getByText('Testimonials');
      expect(testimonialsHeading).toBeInTheDocument();
  
      // Check if the FAQ section is rendered
      const faqHeading = screen.getByText('Frequently Asked Questions');
      expect(faqHeading).toBeInTheDocument();
    });
});

describe('Substances Page', () => {
  test('renders substances correctly', () => {
    render(
      <MemoryRouter>
        <Substances />
      </MemoryRouter>
    );
  });

  test('fetches and displays substances', async () => {
    render(<Substances />);
    const substance = await screen.findByText("Substances");
    expect(substance).toBeInTheDocument();
  });

  test('substances description', async () => {
    render(<Substances />);
    const substance_page = await screen.findByText("There are a wide variety of substances that must be handled responsibly. They can be highly addictive, and therefore, can have dangerous side effects. Click on a card to learn more.");
    expect(substance_page).toBeInTheDocument();
  });
});

describe('Treatment Centers Page', () => {
  test('renders treatment centers correctly', () => {
    render(
      <MemoryRouter>
        <TreatmentCenters />
      </MemoryRouter>
    );
  });

  test('fetches and displays treatment centers', async () => {
    render(<TreatmentCenters />);
    const treatmentCenter = await screen.findByText("Treatment Centers");
    expect(treatmentCenter).toBeInTheDocument();
  });

  test('treatment centers description', async () => {
    render(<TreatmentCenters />);
    const treatment_center_page = await screen.findByText("There are several treatment centers across the country that can help rehabilitate those with substance abuse disorders. Click on a card to learn more.");
    expect(treatment_center_page).toBeInTheDocument();
  });
});

describe('City Statistics Page', () => {
  test('renders cities correctly', () => {
    render(
      <MemoryRouter>
        <Cities />
      </MemoryRouter>
    );
  });

  test('fetches and displays city statistics', async () => {
    render(<Cities />);
    const cities = await screen.findByText(/City Statistics/i);
    expect(cities).toBeInTheDocument();
  });

  afterAll(() => {
    console.clear();
  });
  
});