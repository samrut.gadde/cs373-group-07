import React from 'react';
import { render, screen, waitFor } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import Substances from '../src/pages/Substances/Substances';
import Splash from '../src/pages/Splash/Splash';
import About from '../src/pages/About/About';
import NavbarComponent from '../src/components/Navbar/Navbar';
import '@testing-library/jest-dom';

describe('Substances Page Tests', () => {
    test('renders substances page correctly', () => {
        render(
        <MemoryRouter>
            <Substances />
        </MemoryRouter>
        );
        expect(screen.getByText('Substances')).toBeInTheDocument();
        expect(screen.getByText(/There are a wide variety of substances/i)).toBeInTheDocument();
    });

    test('renders search bar correctly', () => {
        render(
        <MemoryRouter>
            <Substances />
        </MemoryRouter>
        );
        expect(screen.getByPlaceholderText('Search...')).toBeInTheDocument();
    });

    test('renders pagination controls correctly', async () => {
        render(
        <MemoryRouter>
            <Substances />
        </MemoryRouter>
        );
        expect(screen.getByText('Page 1 of results.')).toBeInTheDocument();
    });

    test('renders filter menu correctly', async () => {
        render(
        <MemoryRouter>
            <Substances />
        </MemoryRouter>
        );
        expect(screen.getByLabelText('Filter By:')).toBeInTheDocument();
    });

    test('renders initial substances correctly', async () => {
        render(
        <MemoryRouter>
            <Substances />
        </MemoryRouter>
        );
        await waitFor(() => {
        expect(screen.getByText('No Substances Available.')).toBeInTheDocument();
        });
    });
});

describe('Splash Page Tests', () => {
  test('renders splash page correctly', async () => {
    render(
      <MemoryRouter>
        <Splash />
      </MemoryRouter>
    );
    expect(screen.getByText('A Comprehensive Look at Substance Abuse Across the United States.')).toBeInTheDocument();
  });

  test('renders navigation links correctly', async () => {
    render(
        <MemoryRouter>
            <Splash />
        </MemoryRouter>
    );
    expect(screen.getByText('Substances')).toBeInTheDocument();
    expect(screen.getByText('Treatment Centers')).toBeInTheDocument();
    expect(screen.getByText('City Statistics')).toBeInTheDocument();
    expect(screen.getByText('About')).toBeInTheDocument();
  });

  test('renders search bar correctly', async () => {
        render(
        <MemoryRouter>
            <Splash />
        </MemoryRouter>
        );
        expect(screen.getByPlaceholderText('Search...')).toBeInTheDocument();
    });
});

describe('NavbarComponent Tests', () => {
  test('renders navbar with correct links', async () => {
    render(<NavbarComponent />);
    expect(screen.getByText('Substances')).toBeInTheDocument();
    expect(screen.getByText('Treatment Centers')).toBeInTheDocument();
    expect(screen.getByText('City Statistics')).toBeInTheDocument();
    expect(screen.getByText('About')).toBeInTheDocument();
  });
});

describe('About Page Tests', () => {
  test('renders about page correctly', async () => {
    render(<About />);
    expect(screen.getByText('About SAA (Substance Abuse Awareness)')).toBeInTheDocument();
    expect(screen.getByText('Meet the Team')).toBeInTheDocument();
  });
});
