import unittest
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By

url = "https://www.substance-abuse.me/"

class Tests(unittest.TestCase):
    @classmethod
    def setUpClass(self) -> None:
        options = Options()
        options.add_argument("--headless")
        options.add_argument("--window-size=1920,1080")
        options.add_argument("--disable-dev-shm-usage")
        options.add_argument("--no-sandbox")
        options.add_argument("--start-maximized")
        self.driver = webdriver.Chrome(options=options)
        self.driver.get(url)

    @classmethod
    def tearDownClass(self):
        self.driver.quit()

    def test_splash(self) -> None:
        self.assertIn("Substance-Abuse", self.driver.title)

    def test_splash_extra(self) -> None:
        self.driver.find_element(By.CLASS_NAME, "navbar-brand").click()
        faq = self.driver.find_element(By.CLASS_NAME, 'faq-section')
        self.assertTrue(faq.is_displayed())

    def test_open_about(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "About").click()
        self.assertEqual(self.driver.current_url, url + "about/")

    def test_about_page_content(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "About").click()
        sources_title = "Data Sources"
        sources_link = "findtreatment.gov"
        sources_desc = "A dataset for treatment centers in the United States"
        self.assertIn(sources_title, self.driver.page_source)
        self.assertIn(sources_link, self.driver.page_source)
        self.assertIn(sources_desc, self.driver.page_source)

    def test_open_substances(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Substances").click()
        self.assertEqual(self.driver.current_url, url + "substances/")

    def test_substances_page_content(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Substances").click()
        page_title = "Substances"
        page_desc = "There are a wide variety of substances that must be handled responsibly. They can be highly addictive, and therefore, can have dangerous side effects. Click on a card to learn more."
        self.assertIn(page_title, self.driver.page_source) 
        self.assertIn(page_desc, self.driver.page_source)

    def test_open_tc(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Treatment Centers").click()
        self.assertEqual(self.driver.current_url, url + "centers/")

    def test_tc_page_content(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Treatment Centers").click()
        page_title = "Treatment Centers"
        page_desc = "There are several treatment centers across the country that can help rehabilitate those with substance abuse disorders. Click on a card to learn more."
        self.assertIn(page_title, self.driver.page_source)
        self.assertIn(page_desc, self.driver.page_source)

    def test_open_cs(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "City Statistics").click()
        self.assertEqual(self.driver.current_url, url + "cities/")

    def test_cs_page_content(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "City Statistics").click()
        page_title = "City Statistics"
        page_desc = "There are several cities that may have higher usages of certain substances and thus may have more at-risk populations. Click on a card to learn more."
        self.assertIn(page_title, self.driver.page_source)
        self.assertIn(page_desc, self.driver.page_source)
        
    # Phase 3 tests  
    def test_splash_search(self) -> None:
        search = self.driver.find_element(By.CSS_SELECTOR, 'input.search-bar.rounded-pill')
        self.assertTrue(search.is_displayed())
        
    def test_substances_search(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Substances").click()
        search = self.driver.find_element(By.CSS_SELECTOR, 'input.search-bar.rounded-pill')
        self.assertTrue(search.is_displayed())
        
    def test_cities_search(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "City Statistics").click()
        search = self.driver.find_element(By.CSS_SELECTOR, 'input.search-bar.rounded-pill')
        self.assertTrue(search.is_displayed())
        
    def test_centers_search(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Treatment Centers").click()
        search = self.driver.find_element(By.CLASS_NAME, 'filter-card-container')
        self.assertTrue(search.is_displayed())
        
    def test_substances_filter(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Substances").click()
        search = self.driver.find_element(By.CLASS_NAME, 'filter-card-container')
        self.assertTrue(search.is_displayed())
        
    def test_cities_filter(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "City Statistics").click()
        search = self.driver.find_element(By.CLASS_NAME, 'sub-sort-container')
        self.assertTrue(search.is_displayed())
        
    def test_centers_filter(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Treatment Centers").click()
        search = self.driver.find_element(By.CLASS_NAME, 'filter-card-container')
        self.assertTrue(search.is_displayed())
        
    def test_substances_pagination(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Substances").click()
        page_num = "Page 1 of results."
        self.assertIn(page_num, self.driver.page_source)
        
    def test_cities_pagination(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "City Statistics").click()
        page_num = "Page 1 of results."
        self.assertIn(page_num, self.driver.page_source)
        
    def test_centers_pagination(self) -> None:
        self.driver.find_element(By.LINK_TEXT, "Treatment Centers").click()
        page_num = "Page 1 of results."
        self.assertIn(page_num, self.driver.page_source)
