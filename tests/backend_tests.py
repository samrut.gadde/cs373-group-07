import unittest
import requests

url = "https://x4c6wslv77.execute-api.us-east-1.amazonaws.com/staging"

class TestCitiesEndpoint(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_cities_endpoint(self):
        response = requests.get(url + "/cities")
        self.assertEqual(response.status_code, 200)

    def test_get_centers_endpoint(self):
        response = requests.get(url + "/cities/getCenters?city=Houston")
        self.assertEqual(response.status_code, 200)

    def test_get_cities_count_endpoint(self):
        response = requests.get(url + "/cities/count")
        self.assertEqual(response.status_code, 200)

    def test_pagination(self):
        response = requests.get(url + "/cities?page=2&pageSize=10")
        self.assertEqual(response.status_code, 200)


class TestCentersEndpoint(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_all_centers_endpoint(self):
        response = requests.get(url + "/centers")
        self.assertEqual(response.status_code, 200)

    def test_get_center_by_id(self):
        response = requests.get(url + "/centers?id=1")
        self.assertEqual(response.status_code, 200)

    def test_get_centers_count_endpoint(self):
        response = requests.get(url + "/centers/count")
        self.assertEqual(response.status_code, 200)

    def test_pagination(self):
        response = requests.get(url + "/centers?page=2&pageSize=10")
        self.assertEqual(response.status_code, 200)


class TestSubstancesEndpoint(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        pass

    def test_get_all_substances_endpoint(self):
        response = requests.get(url + "/substances")
        self.assertEqual(response.status_code, 200)

    def test_get_substances_count_endpoint(self):
        response = requests.get(url + "/substances/count")
        self.assertEqual(response.status_code, 200)

    def test_pagination(self):
        response = requests.get(url + "/substances?page=2&pageSize=10")
        self.assertEqual(response.status_code, 200)


if __name__ == "__main__":
    unittest.main()
