import awsgi
import os
from flask_cors import CORS
from flask import Flask, jsonify, request
import psycopg2
import logging
import sys

logging.basicConfig(level=logging.INFO)
app = Flask(__name__)
app.json.sort_keys = False
CORS(app)

BASE_ROUTE = "/substances"

DB_NAME = os.environ.get("DATABASE_NAME")
DB_PORT = os.environ.get("DATABASE_PORT")
DB_USER = os.environ.get("DATABASE_USER")
DB_PASSWORD = os.environ.get("DATABASE_PASSWORD")
DB_HOST = os.environ.get("DATABASE_HOST")

DEFAULT_PAGE_SIZE = 100
DEFAULT_PAGE = 1
MAXIMUM_PAGE_SIZE = 500

try:
    conn = psycopg2.connect(
        dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT
    )
except psycopg2.Error as e:
    logging.error(
        "ERROR: Unexpected error: \
                    Could not connect to Postgres instance."
    )
    logging.error(e)
    sys.exit()

logging.info("SUCCESS: Connection to RDS Postgres instance succeeded")

@app.route(BASE_ROUTE, methods=["GET"])
def get_substances():
    """
    Get all substances from the database
    """

    # get query parameters
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)
    category = request.args.get("category")
    name = request.args.get("name")
    sort = request.args.get("sort")
    query = request.args.get("query")

    if pageSize < 1 or pageSize > MAXIMUM_PAGE_SIZE:
        return jsonify(
            {
                "error": "Invalid pageSize. Must be between 1 and "
                + str(MAXIMUM_PAGE_SIZE)
            }
        )

    if page < 1:
        return jsonify({"error": "Invalid page. Must be greater than 0"})
    
    if name:
        return get_substance_by_name(name)

    if query:
        return get_substances_by_search(query, page, pageSize)
    
    if category:
        return get_substances_of_category(category, page, pageSize)
    
    if sort and (sort != "A-Z" and sort != "Z-A"):
        return jsonify({"error": "Invalid sort parameter"})

    with conn.cursor() as cur:
        if not sort:
            cur.execute(
                "SELECT * FROM substances LIMIT %s OFFSET %s",
                (pageSize, (page - 1) * pageSize),
            )
        elif sort == "A-Z":
            cur.execute(
                "SELECT * FROM substances ORDER BY name LIMIT %s OFFSET %s",
                (pageSize, (page - 1) * pageSize),
            )
        else:
            cur.execute(
                "SELECT * FROM substances ORDER BY name DESC LIMIT %s OFFSET %s",
                (pageSize, (page - 1) * pageSize),
            )
        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        substances = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(substances)

def get_substances_of_category(category, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    if not category or (category != "Sedative" and category != "Stimulant" and category != "Depressant" and category != "Other"):
        return jsonify({"error": "No category parameter or invalid category parameter sent in"})
    
    with conn.cursor() as cur:

        if category == "Depressant":
            cur.execute(
                "SELECT * FROM substances WHERE category = %s LIMIT %s OFFSET %s",
                (category, pageSize, (page - 1) * pageSize),
            )
        elif category == "Stimulant":
            cur.execute(
                "SELECT * FROM substances WHERE category = %s LIMIT %s OFFSET %s",
                (category, pageSize, (page - 1) * pageSize),
            )
        elif category == "Other":
            cur.execute(
                "SELECT * FROM substances WHERE category = %s LIMIT %s OFFSET %s",
                (category, pageSize, (page - 1) * pageSize),
            )
        elif category == "Sedative":
            cur.execute(
                "SELECT * FROM substances WHERE category = %s LIMIT %s OFFSET %s",
                (category, pageSize, (page - 1) * pageSize),
            )
        else:
            cur.execute(
                "SELECT * FROM substances WHERE category IN (%s, %s) LIMIT %s OFFSET %s",
                ('Stimulants', 'Other', pageSize, (page - 1) * pageSize),
            )



        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        substances = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(substances)

def get_substance_by_name(name):
    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM substances WHERE name = %s",
            (name,),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        substances = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(substances)


@app.route(BASE_ROUTE + "/count", methods=["GET"])
def get_substances_count():
    """
    Get count of substances from the database
    """
    
    query = request.args.get("query")
    category = request.args.get("category")
    
    if query:
        query = query.lower()
        query = "%" + query + "%"
        with conn.cursor() as cur:
            cur.execute(
                "SELECT COUNT(*) FROM substances WHERE concat(lower(name), ' ', lower(indication), ' ' , lower(products), ' ', lower(categories)) like %s",
                (query,),
            )

            count = cur.fetchone()

        conn.commit()
        return jsonify({"count": count})
    if category and (category == "Sedative" or category == "Stimulant" or category == "Depressant" or category == "Other"):
        with conn.cursor() as cur:
            cur.execute(
                "SELECT COUNT(*) FROM substances WHERE category = %s",
                (category,),
            )

            count = cur.fetchone()

        conn.commit()
        return jsonify({"count": count})
    elif category:
        return jsonify({"error": "Invalid category parameter sent in"})
    

    with conn.cursor() as cur:
        cur.execute("SELECT COUNT(*) FROM substances")

        count = cur.fetchone()

    conn.commit()
    return jsonify({"count": count})


def get_substances_by_search(query, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Perform a full-text search on the substances table
    """
    
    
    if not query:
        return jsonify({"error": "No query parameter sent in"})

    query = query.lower()
    query = "%" + query + "%"
    
    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM substances where concat(lower(name), ' ', lower(indication), ' ' , lower(products), ' ', lower(categories)) like %s LIMIT %s OFFSET %s",
            (query, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        substances = [dict(zip(column_names, row)) for row in cur.fetchall()]
        
    conn.commit()
    return jsonify(substances)

def handler(event, context):
    return awsgi.response(app, event, context)
