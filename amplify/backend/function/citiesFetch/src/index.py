import awsgi
import os
from flask_cors import CORS
from flask import Flask, jsonify, request
import psycopg2
from psycopg2.sql import SQL, Identifier
import logging
import sys

logging.basicConfig(level=logging.INFO)
app = Flask(__name__)
app.json.sort_keys = False
CORS(app)

BASE_ROUTE = "/cities"

DB_NAME = os.environ.get("DATABASE_NAME")
DB_PORT = os.environ.get("DATABASE_PORT")
DB_USER = os.environ.get("DATABASE_USER")
DB_PASSWORD = os.environ.get("DATABASE_PASSWORD")
DB_HOST = os.environ.get("DATABASE_HOST")

DEFAULT_PAGE_SIZE = 100
DEFAULT_PAGE = 1
MAXIMUM_PAGE_SIZE = 500

try:
    conn = psycopg2.connect(
        dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT
    )
except psycopg2.Error as e:
    logging.error(
        "ERROR: Unexpected error: \
                    Could not connect to Postgres instance."
    )
    logging.error(e)
    sys.exit()

logging.info("SUCCESS: Connection to RDS Postgres instance succeeded")


@app.route(BASE_ROUTE, methods=["GET"])
def get_cities():
    """
    Get all cities from the database
    """

    # get query parameters
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)
    state = request.args.get("state")
    name = request.args.get("name")
    leading_category = request.args.get("leadingcategory")

    if pageSize < 1 or pageSize > MAXIMUM_PAGE_SIZE:
        return jsonify(
            {
                "error": "Invalid pageSize. Must be between 1 and "
                + str(MAXIMUM_PAGE_SIZE)
            }
        )

    if page < 1:
        return jsonify({"error": "Invalid page. Must be greater than 0"})

    if state:
        return get_city_by_state(state, page, pageSize)

    if name:
        return get_city_by_name(name, page, pageSize)

    if leading_category:
        return get_cities_by_leading_category(leading_category, page, pageSize)

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM cities LIMIT %s OFFSET %s",
            (pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(cities)


def get_city_by_state(state, page, pageSize):
    """
    Get cities of given state from the database
    """

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM cities WHERE state = %s LIMIT %s OFFSET %s",
            (state, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]

        if not cities:
            return jsonify({"error": "No cities found for state " + state})

    conn.commit()
    return jsonify(cities)


def get_city_by_name(name, page, pageSize):
    """
    Get cities of given name from the database
    """

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM cities WHERE city = %s LIMIT %s OFFSET %s",
            (name, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]

        if not cities:
            return jsonify({"error": "No cities found for name " + name})

    conn.commit()
    return jsonify(cities)


def get_cities_by_leading_category(leading_category, page, pageSize):
    """
    Get cities of given leading category from the database
    """

    if not leading_category or (
        leading_category != "Stimulant" and leading_category != "Depressant"
    ):
        return jsonify(
            {
                "error": "No leading category parameter or invalid leading category parameter sent in"
            }
        )

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM cities WHERE leadingcategory = %s LIMIT %s OFFSET %s",
            (leading_category, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]

        if not cities:
            return jsonify(
                {"error": "No cities found for leading category " + leading_category}
            )

    conn.commit()
    return jsonify(cities)

@app.route(BASE_ROUTE + "/queryCities", methods=["GET"])
def query_cities():
    """
    Perform a full-text search on the cities table
    """
    query = request.args.get("query")
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)
    if not query:
        return jsonify({"error": "No query parameter sent in"})

    query = query.lower()
    query = "%" + query + "%"
    
    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM cities where concat(lower(city), ' ', lower(state), ' ' , marijuana, ' ', cocaine, ' ', meth, ' ', heroin, ' ', lower(region), ' ', lower(leadingcategory)) like %s LIMIT %s OFFSET %s",
            (query, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]
        
    conn.commit()
    return jsonify(cities)
@app.route(BASE_ROUTE + "/sortFilterCities", methods=["GET"])
def sort_filter_cities():
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)
    filterColumn = request.args.get("filterColumn")
    filterVal = request.args.get("filterVal")
    sortColumn = request.args.get("sortColumn")
    sortDirection = request.args.get("sortDirection")

    if pageSize < 1 or pageSize > MAXIMUM_PAGE_SIZE:
        return jsonify(
            {
                "error": "Invalid pageSize. Must be between 1 and "
                + str(MAXIMUM_PAGE_SIZE)
            }
        )

    if page < 1:
        return jsonify({"error": "Invalid page. Must be greater than 0"})
    
    if sortColumn and sortDirection != "ascending" and sortDirection != "descending":
        return jsonify({"error": "Invalid sort direction. Must be 'ascending' or 'descending'"})
    
    with conn.cursor() as cur:
        if not sortColumn and not filterColumn:
            cur.execute(
            "SELECT * FROM cities LIMIT %s OFFSET %s",
            (pageSize, (page - 1) * pageSize),
            )   
        elif not sortColumn:
            #case where its just filter
            cur.execute(
            # "SELECT * FROM cities WHERE %s = %s LIMIT %s OFFSET %s",
            # (filterColumn, filterVal, pageSize, (page - 1) * pageSize),
                SQL("SELECT * FROM cities WHERE {} = %s LIMIT %s OFFSET %s")
                    .format(Identifier(filterColumn)), (filterVal, pageSize, (page - 1) * pageSize)
            )
        elif not filterColumn:
            #case where its just sort
            if sortDirection == "ascending":
                cur.execute(
                    # "SELECT * FROM cities ORDER BY %s LIMIT %s OFFSET %s",
                    # (
                    #     sortColumn, pageSize, (page - 1) * pageSize
                    # ),
                    SQL("SELECT * FROM cities ORDER BY {} LIMIT %s OFFSET %s")
                    .format(Identifier(sortColumn)), (pageSize, (page - 1) * pageSize)
                )
            else:
                cur.execute(
                    SQL("SELECT * FROM cities ORDER BY {} DESC LIMIT %s OFFSET %s")
                    .format(Identifier(sortColumn)), (pageSize, (page - 1) * pageSize)
                )
        else:
            # case for both filter and sort
            if sortDirection == "ascending":
                cur.execute(
                    # "SELECT * FROM cities WHERE %s = %s ORDER BY %s LIMIT %s OFFSET %s",
                    # (
                    #     filterColumn,
                    #     filterVal,
                    #     sortColumn,
                    #     pageSize,
                    #     (page - 1) * pageSize,
                    # ),
                    SQL("SELECT * FROM cities WHERE {} = %s ORDER BY {} LIMIT %s OFFSET %s")
                    .format(Identifier(filterColumn), Identifier(sortColumn)), (filterVal, pageSize, (page - 1) * pageSize)
                )
            else:
                cur.execute(
                    # "SELECT * FROM cities WHERE %s = %s ORDER BY %s DESC LIMIT %s OFFSET %s",
                    # (
                    #     filterColumn,
                    #     filterVal,
                    #     sortColumn,
                    #     pageSize,
                    #     (page - 1) * pageSize,
                    # ),
                    SQL("SELECT * FROM cities WHERE {} = %s ORDER BY {} DESC LIMIT %s OFFSET %s")
                    .format(Identifier(filterColumn), Identifier(sortColumn)), (filterVal, pageSize, (page - 1) * pageSize)
                )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        cities = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(cities)


@app.route(BASE_ROUTE + "/getCenters", methods=["GET"])
def get_centers():
    """
    Get centers of given city from the database
    """

    # get query parameters
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)
    city = request.args.get("city")

    if pageSize < 1 or pageSize > MAXIMUM_PAGE_SIZE:
        return jsonify(
            {
                "error": "Invalid pageSize. Must be between 1 and "
                + str(MAXIMUM_PAGE_SIZE)
            }
        )

    if page < 1:
        return jsonify({"error": "Invalid page. Must be greater than 0"})

    if not city:
        return jsonify({"error": "No city parameter sent in"})

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM centers WHERE city = %s LIMIT %s OFFSET %s",
            (city, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


@app.route(BASE_ROUTE + "/count", methods=["GET"])
def get_cities_count():
    """
    Get count of cities from the database
    """

    leading_category = request.args.get("leadingcategory")

    if leading_category:
        return get_cities_count_by_leading_category(leading_category)

    with conn.cursor() as cur:
        cur.execute("SELECT COUNT(*) FROM cities")

        count = cur.fetchone()

    conn.commit()
    return jsonify({"count": count})


def get_cities_count_by_leading_category(leading_category):
    """
    Get count of cities of given leading category from the database
    """

    if not leading_category or (
        leading_category != "Stimulant" and leading_category != "Depressant"
    ):
        return jsonify(
            {
                "error": "No leading category parameter or invalid leading category parameter sent in"
            }
        )

    with conn.cursor() as cur:
        cur.execute(
            "SELECT COUNT(*) FROM cities WHERE leadingcategory = %s",
            (leading_category,),
        )

        count = cur.fetchone()

    conn.commit()
    return jsonify({"count": count})


def handler(event, context):
    return awsgi.response(app, event, context)
