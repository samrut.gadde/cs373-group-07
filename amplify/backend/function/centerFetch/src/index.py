import awsgi
import os
from flask_cors import CORS
from flask import Flask, jsonify, request
import psycopg2
import logging
import sys

logging.basicConfig(level=logging.INFO)
app = Flask(__name__)
app.json.sort_keys = False
CORS(app)

BASE_ROUTE = "/centers"

DB_NAME = os.environ.get("DATABASE_NAME")
DB_PORT = os.environ.get("DATABASE_PORT")
DB_USER = os.environ.get("DATABASE_USER")
DB_PASSWORD = os.environ.get("DATABASE_PASSWORD")
DB_HOST = os.environ.get("DATABASE_HOST")

DEFAULT_PAGE_SIZE = 100
DEFAULT_PAGE = 1
MAXIMUM_PAGE_SIZE = 500

try:
    conn = psycopg2.connect(
        dbname=DB_NAME, user=DB_USER, password=DB_PASSWORD, host=DB_HOST, port=DB_PORT
    )
except psycopg2.Error as e:
    logging.error(
        "ERROR: Unexpected error: \
                    Could not connect to Postgres instance."
    )
    logging.error(e)
    sys.exit()

logging.info("SUCCESS: Connection to RDS Postgres instance succeeded")


@app.route(BASE_ROUTE, methods=["GET"])
def get_all_centers():
    """
    Get all centers from the database
    """

    id = request.args.get("id")
    name1 = request.args.get("name1")
    name2 = request.args.get("name2")
    if id or (name1 and name2):
        return get_center(id)
    elif name1 and not name2 or name2 and not name1:
        return jsonify({"error": "Invalid name1, name2. Must be provided"})

    category = request.args.get("category")
    longitude = request.args.get("longitude")
    latitude = request.args.get("latitude")
    ownership = request.args.get("ownership")
    payment = request.args.get("payment")
    testing = request.args.get("testing")
    query = request.args.get("query")

    # get query parameters
    page = request.args.get("page", default=DEFAULT_PAGE, type=int)
    pageSize = request.args.get("pageSize", default=DEFAULT_PAGE_SIZE, type=int)

    if pageSize < 1 or pageSize > MAXIMUM_PAGE_SIZE:
        return jsonify(
            {
                "error": "Invalid pageSize. Must be between 1 and "
                + str(MAXIMUM_PAGE_SIZE)
            }
        )

    if page < 1:
        return jsonify({"error": "Invalid page. Must be greater than 0"})

    if category:
        return get_centers_of_category(category, page, pageSize)

    if longitude and latitude:
        return get_centers_sorted_by_distance(longitude, latitude, page, pageSize)

    if ownership:
        return get_centers_by_ownership(ownership, page, pageSize)

    if payment:
        return get_centers_by_payment(payment, page, pageSize)

    if testing:
        return get_centers_by_testing(testing, page, pageSize)

    if query:
        return get_centers_by_search(query, page, pageSize)

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM centers LIMIT %s OFFSET %s",
            (pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_center(id=None, name1=None, name2=None):
    """
    Get a center from the database by name
    """

    id = request.args.get("id")
    name1 = request.args.get("name1")
    name2 = request.args.get("name2")

    if id:
        with conn.cursor() as cur:
            cur.execute(
                "SELECT * FROM centers WHERE id = %s",
                (id),
            )

            column_names = [desc[0] for desc in cur.description]
            logging.info(column_names)
            center = [dict(zip(column_names, row)) for row in cur.fetchall()]
    elif name1 and name2:
        with conn.cursor() as cur:
            cur.execute(
                "SELECT * FROM centers WHERE name1 = %s AND name2 = %s",
                (name1, name2),
            )

            column_names = [desc[0] for desc in cur.description]
            logging.info(column_names)
            center = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(center)


def get_centers_of_category(category, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Get centers of given category from the database
    """

    if not category or (category != "Stimulant" and category != "Depressant"):
        return jsonify(
            {"error": "No category parameter or invalid category parameter sent in"}
        )

    with conn.cursor() as cur:
        cur.execute(
            "SELECT * FROM centers WHERE category = %s LIMIT %s OFFSET %s",
            (category, pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_sorted_by_name():
    """
    Get centers sorted by name from the database
    """

    with conn.cursor() as cur:
        cur.execute("SELECT * FROM centers ORDER BY name1, name2")

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_sorted_by_distance(
    user_longitude, user_latitude, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE
):
    """
    Get the closest centers to the user's location
    """

    with conn.cursor() as cur:
        cur.execute(
            """
            SELECT *, 
            (
                6371 * acos(
                    cos(radians(%s)) * cos(radians(latitude)) * cos(radians(longitude) - radians(%s)) +
                    sin(radians(%s)) * sin(radians(latitude))
                )
            ) AS distance
            FROM centers
            ORDER BY distance
            LIMIT %s OFFSET %s
            """,
            (
                user_latitude,
                user_longitude,
                user_latitude,
                pageSize,
                (page - 1) * pageSize,
            ),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_by_ownership(ownership, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Get centers by ownership (private, public) from the database
    """

    if not ownership or (ownership != "Public" and ownership != "Private"):
        return jsonify(
            {"error": "No ownership parameter or invalid ownership parameter sent in"}
        )

    cols = []
    if ownership == "Public":
        cols.extend(["LCCG", "STG", "FED", "VAMC", "DDF"])
    elif ownership == "Private":
        cols.extend(["PVTP", "PVTN", "IH", "TBG"])

    query = (
        "SELECT * FROM centers WHERE "
        + " OR ".join([f"{col}=True" for col in cols])
        + " LIMIT %s OFFSET %s"
    )

    with conn.cursor() as cur:
        cur.execute(
            query,
            (pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_by_payment(payment, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Get centers by payment (Medicare, Medicaid, Military Insurance) from the database
    """

    if not payment or (
        payment != "Medicare"
        and payment != "Medicaid"
        and payment != "Military Insurance"
        and payment != "No Payment Accepted"
        and payment != "Private Health Insurance"
    ):
        return jsonify(
            {"error": "No payment parameter or invalid payment parameter sent in"}
        )

    cols = []
    if payment == "Medicare":
        cols.append("MC")
    elif payment == "Medicaid":
        cols.append("MD")
    elif payment == "Military Insurance":
        cols.append("MI")
    elif payment == "No Payment Accepted":
        cols.append("NP")
    elif payment == "Private Health Insurance":
        cols.append("PI")

    query = (
        "SELECT * FROM centers WHERE "
        + " OR ".join([f"{col}=True" for col in cols])
        + " LIMIT %s OFFSET %s"
    )

    with conn.cursor() as cur:
        cur.execute(
            query,
            (pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_by_testing(testing, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Get centers by testing (Drug, Alcohol) from the database
    """

    if not testing or (testing != "Drug" and testing != "Alcohol"):
        return jsonify(
            {"error": "No testing parameter or invalid testing parameter sent in"}
        )

    cols = []
    if testing == "Drug":
        cols.extend(["DAUT", "DAOF"])
    elif testing == "Alcohol":
        cols.append("BABA")

    query = (
        "SELECT * FROM centers WHERE "
        + " OR ".join([f"{col}=True" for col in cols])
        + " LIMIT %s OFFSET %s"
    )
    print(query)

    with conn.cursor() as cur:
        cur.execute(
            query,
            (pageSize, (page - 1) * pageSize),
        )

        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


def get_centers_by_search(query, page=DEFAULT_PAGE, pageSize=DEFAULT_PAGE_SIZE):
    """
    Perform a full-text search on the centers table
    """

    if not query:
        return jsonify({"error": "No query parameter sent in"})

    query = query.lower()
    query = "%" + query + "%"

    with conn.cursor() as cur:
        cur.execute(
            """
                SELECT * FROM centers 
                WHERE concat(
                    lower(name1), ' ', lower(name2), ' ', lower(phone),
                    ' ', lower(street1), ' ', lower(city), ' ', lower(state),
                    ' ', lower(zip))
                LIKE %s LIMIT %s OFFSET %s
            """,
            (
                query, pageSize, (page - 1) * pageSize
            ),
        )
        
        column_names = [desc[0] for desc in cur.description]
        logging.info(column_names)
        centers = [dict(zip(column_names, row)) for row in cur.fetchall()]

    conn.commit()
    return jsonify(centers)


@app.route(BASE_ROUTE + "/count", methods=["GET"])
def get_number_of_centers():
    """
    Get the number of centers in the database
    """

    category = request.args.get("category")
    ownership = request.args.get("ownership")
    payment = request.args.get("payment")
    testing = request.args.get("testing")
    query = request.args.get("query")
    longitude = request.args.get("longitude")
    latitude = request.args.get("latitude")

    if category:
        return get_number_of_centers_of_category(category)

    if ownership:
        return get_number_of_centers_by_ownership(ownership)

    if payment:
        return get_number_of_centers_by_payment(payment)

    if testing:
        return get_number_of_centers_by_testing(testing)

    if query:
        return get_number_of_centers_by_search(query)

    if longitude and latitude:
        return get_number_of_centers_by_location(longitude, latitude)

    with conn.cursor() as cur:
        cur.execute("SELECT COUNT(*) FROM centers")
        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_of_category(category):
    """
    Get the number of centers of given category in the database
    """

    if not category or (category != "Stimulant" and category != "Depressant"):
        return jsonify(
            {"error": "No category parameter or invalid category parameter sent in"}
        )

    with conn.cursor() as cur:
        cur.execute(
            "SELECT COUNT(*) FROM centers WHERE category = %s",
            (category,),
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_by_ownership(ownership):
    """
    Get the number of centers by ownership (private, public) in the database
    """

    if not ownership or (ownership != "Public" and ownership != "Private"):
        return jsonify(
            {"error": "No ownership parameter or invalid ownership parameter sent in"}
        )

    cols = []
    if ownership == "Public":
        cols.extend(["LCCG", "STG", "FED", "VAMC", "DDF"])
    elif ownership == "Private":
        cols.extend(["PVTP", "PVTN", "IH", "TBG"])

    query = "SELECT COUNT(*) FROM centers WHERE " + " OR ".join(
        [f"{col}=True" for col in cols]
    )

    with conn.cursor() as cur:
        cur.execute(
            query,
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_by_payment(payment):
    """
    Get the number of centers by payment (Medicare, Medicaid, Military Insurance) in the database
    """

    if not payment or (
        payment != "Medicare"
        and payment != "Medicaid"
        and payment != "Military Insurance"
        and payment != "No Payment Accepted"
        and payment != "Private Health Insurance"
    ):
        return jsonify(
            {"error": "No payment parameter or invalid payment parameter sent in"}
        )

    cols = []
    if payment == "Medicare":
        cols.append("MC")
    elif payment == "Medicaid":
        cols.append("MD")
    elif payment == "Military Insurance":
        cols.append("MI")
    elif payment == "No Payment Accepted":
        cols.append("NP")
    elif payment == "Private Health Insurance":
        cols.append("PI")

    query = "SELECT COUNT(*) FROM centers WHERE " + " OR ".join(
        [f"{col}=True" for col in cols]
    )

    with conn.cursor() as cur:
        cur.execute(
            query,
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_by_testing(testing):
    """
    Get the number of centers by testing (Drug, Alcohol) in the database
    """

    if not testing or (testing != "Drug" and testing != "Alcohol"):
        return jsonify(
            {"error": "No testing parameter or invalid testing parameter sent in"}
        )

    cols = []
    if testing == "Drug":
        cols.extend(["DAUT", "DAOF"])
    elif testing == "Alcohol":
        cols.append("BABA")

    query = "SELECT COUNT(*) FROM centers WHERE " + " OR ".join(
        [f"{col}=True" for col in cols]
    )

    with conn.cursor() as cur:
        cur.execute(
            query,
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_by_search(query):
    """
    Get the number of centers by search query in the database
    """

    if not query:
        return jsonify({"error": "No query parameter sent in"})
    
    query = query.lower()
    query = "%" + query + "%"

    with conn.cursor() as cur:
        cur.execute(
            """
                SELECT COUNT(*) FROM centers 
                WHERE concat(
                    lower(name1), ' ', lower(name2), ' ', lower(phone),
                    ' ', lower(street1), ' ', lower(city), ' ', lower(state),
                    ' ', lower(zip))
                LIKE %s
            """,
            (query,),
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def get_number_of_centers_by_location(user_longitude, user_latitude):
    """
    Get the number of centers by location in the database
    """

    with conn.cursor() as cur:
        cur.execute(
            """
            SELECT COUNT(*)
            FROM centers
            WHERE 6371 * acos(
                cos(radians(%s)) * cos(radians(latitude)) * cos(radians(longitude) - radians(%s)) +
                sin(radians(%s)) * sin(radians(latitude))
            ) < 100
            """,
            (user_latitude, user_longitude, user_latitude),
        )

        count = cur.fetchone()[0]

    conn.commit()
    return jsonify({"count": count})


def handler(event, context):
    return awsgi.response(app, event, context)
