'''
This script is used to insert the data from the centers csv file into the database
'''
import csv
import psycopg2
import logging
import sys

import os
from dotenv import load_dotenv, find_dotenv
# if __name__ == '__main__':
def __main__():
  debug = 0
  load_dotenv(find_dotenv())

  logging.basicConfig(level=logging.INFO)

  with open('public/centerData.csv', 'r') as file:
    csv_reader = csv.DictReader(file)
    
    data = []
    
    num = 0
    for row in csv_reader:
      data.append(row)
      num += 1
      if num % 100 == 0:
        logging.info("Read " + str(num) + " rows")
    
  # retrieve db info from environment variables
  DB_NAME = os.environ.get('DB_NAME')
  DB_PORT = os.environ.get('DB_PORT')
  DB_USER = os.environ.get('DB_USER')
  DB_PASSWORD = os.environ.get('DB_PASS')
  DB_HOST = os.environ.get('DB_HOST')

  # for local db testing
  # DB_NAME = 'postgres'
  # DB_PORT = 5432
  # DB_USER = 'postgres'
  # DB_PASSWORD = 'password'
  # DB_HOST = 'localhost'

  logging.info("DB_NAME: " + DB_NAME)

  try:
      conn = psycopg2.connect(
          dbname=DB_NAME,
          user=DB_USER,
          password=DB_PASSWORD,
          host=DB_HOST,
          port=DB_PORT
      )
  except psycopg2.Error as e:
      logging.error("ERROR: Unexpected error: Could not connect to Postgres instance.")
      logging.error(e)
      sys.exit()

  logging.info("SUCCESS: Connection to RDS Postgres instance succeeded")

  # drop table if exists
  cur = conn.cursor()
  # cur.execute("DROP TABLE IF EXISTS centers;")
  # conn.commit()

  # # # create table based on keys in the first row of the csv
  services = "sa","dt","mm","dm","bum","db","rpn","bu","nxn","vtrl","meth","hh","noop","pain","nmoa","moa","ubn","otp","cbt","dbt","tele","saca","trc","smon","smpd","smop","hi","res","op","rs","rl","rd","od","omb","odt","oit","ort","hid","hit","ct","gh","psyh","vamc","tbg","ih","stg","lccg","ddf","stag","stmh","stdh","hla","jc","carf","ncqa","coa","hfap","np","sf","md","mc","si","pi","mi","fsa","ss","pa","ah","sp","co","gl","vet","adm","mf","cj","se","ad","pw","wn","mn","hv","trma","xa","dv","tay","nsc","adtx","bdtx","cdtx","mdtx","odtx","tgd","ico","gco","fco","mco","twfa","bia","cmi","moti","ang","mxm","crv","relp","bc","chld","yad","adlt","fem","male","du","duo","acc","acm","acu","baba","ccc","cmha","csaa","daut","dp","dsf","dvfp","eih","emp","haec","heoh","hivt","isc","itu","mhs","opc","sae","shg","smhd","ssa","ssd","stdt","ta","taec","tbs","cm","fpsy","hs","nrt","peer","stu","tcc","bsdm","nu","mu","bwn","bwon","ub","un","beri","pvtp","pvtn","sumh","inpe","rpe","pc","naut","nmaut","acma","pmat","auinpe","aurpe","aupc","dlc","mhiv","mhcv","lfxd","clnd","copsu","daof","mst","noe","ofd","rc","piec","mdet","voc","hav","hbv","audo","mmd","mpep","fed","sotp","dea","samf","mhpa","hbt","hct","tod","vapn","vapp","vppd","oudo","nx","fx","mws","f17","f19","f25","f28","f30","f31","f35","f36","f37","f4","f42","f43","f47","f66","f67","f70","f81","f92","n24","n40"
  create_table = "CREATE TABLE centers (id serial PRIMARY KEY,\
      name1 varchar, name2 varchar, street1 varchar, street2 varchar,\
      city varchar, state varchar, zip varchar, zip4 varchar,\
      county varchar, phone varchar, intake_prompt varchar,\
      intake1 varchar, intake2 varchar, website varchar, latitude float,\
      longitude float, type_facility varchar"
    


  serv_iter = iter(services)
  create_table += ", " + next(serv_iter) + " boolean"
  for service in serv_iter:
    create_table += ", " + service + " boolean"

  create_table += ");"

  if debug:
    print(create_table)
  # cur.execute(create_table)

  # get cities from db
  try:
    cur.execute("SELECT * FROM cities;")
    cities = cur.fetchall()
  except ValueError as e:
    logging.error("Error fetching cities from db")
    logging.error(e)
    sys.exit()
    
  city_names = [t[1] for t in cities]
    
  # delete centers that are not in cities from db
  for row in data:
    if row['city'] not in city_names:
      cur.execute("DELETE FROM centers WHERE city = %s;", (row['city'],))
      logging.info("Deleted centers for city: " + row['city'])



  # insert data only for cities that are in the db

  # first modify services to be boolean
  for row in data:
    for service in services:
      if row[service] == "1":
        row[service] = True
      else:
        row[service] = False


  # then insert data
  # num = 0
  # for row in data:
  #   keys = list(row.keys())
  #   values = list(row.values())
  #   # logging.info("keys: " + str(keys))
  #   # logging.info("values: " + str(values))
  #   insert = "INSERT INTO centers1 ("
  #   insert += ", ".join(keys)
  #   insert += ") VALUES ("
  #   val_iter = iter(values)
  #   insert += '\''+str(next(val_iter))+ '\''
  #   for val in val_iter:
  #     if str(val) is "":
  #       insert += ", NULL"
  #     else:
  #       insert_str = str(val)
  #       if "\'" in insert_str:
  #         insert_str = insert_str.replace("\'", "\'\'")
  #       insert += ", " + '\''+ insert_str + '\''
      
  #   insert += ");"
  #   print(insert)
    
  #   try: 
  #     cur.execute(insert, values)
  #   except ValueError as e:
  #     logging.error("Error inserting data")
  #     logging.error(e)
  #     logging.error(insert)
  #     sys.exit()
  #   num += 1
  #   if num % 100 == 0:
  #     logging.info("Inserted " + str(num) + " rows")
    

  conn.commit()
  logging.info("Data inserted")

if __name__ == '__main__':
    __main__()



