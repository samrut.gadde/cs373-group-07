import csv
import psycopg2
import logging
import sys

import os
from dotenv import load_dotenv, find_dotenv
# if __name__ == '__main__':
def __main__():
    debug = 0
    load_dotenv(find_dotenv())

    logging.basicConfig(level=logging.INFO)

    try:
        with open('public/substancesCSV.csv', 'r', encoding = 'utf-8') as file:
            csv_reader = csv.DictReader(file)
            
            data = []
            
            num = 0
            for row in csv_reader:
                data.append(row)
                num += 1
                if num % 20 == 0:
                    logging.info("Read " + str(num) + " rows")
        DB_NAME = os.environ.get('DB_NAME')
        DB_PORT = os.environ.get('DB_PORT')
        DB_USER = os.environ.get('DB_USER')
        DB_PASSWORD = os.environ.get('DB_PASS')
        DB_HOST = os.environ.get('DB_HOST')
        
        logging.info("DB_NAME: " + DB_NAME)
        try:
            conn = psycopg2.connect(
                dbname=DB_NAME,
                user=DB_USER,
                password=DB_PASSWORD,
                host=DB_HOST,
                port=DB_PORT
            )
        except psycopg2.Error as e:
            logging.error("ERROR: Unexpected error: Could not connect to Postgres instance.")
            logging.error(e)
            sys.exit()

        logging.info("SUCCESS: Connection to RDS Postgres instance succeeded")
        
        #print(data)
        cur = conn.cursor()
        cur.execute("DROP TABLE IF EXISTS substances;")
        conn.commit()

        # # # create table based on keys in the first row of the csv
        create_table = "CREATE TABLE substances (id serial PRIMARY KEY,\
            Name varchar, Description varchar, Indication varchar, Toxicity varchar,\
            Products varchar, Categories varchar, Category varchar);"
        
        cur.execute(create_table)
        if debug:
            print("table created")
        # insert data

        num = 0
        for row in data:
            keys = list(row.keys())
            values = list(row.values())
            logging.info("keys: " + str(keys))
            logging.info("values: " + str(values))
            insert = "INSERT INTO substances ("
            insert += ", ".join(keys)
            insert += ") VALUES ("
            val_iter = iter(values)
            insert += '\''+str(next(val_iter))+ '\''
            for val in val_iter:
                if val == "":
                    insert += ", NULL"
                else:
                    insert_str = val
                    if "\'" in insert_str:
                        insert_str = insert_str.replace("\'", "\'\'")
                    if "%" in insert_str:
                        insert_str = insert_str.replace("%", "%%")
                    insert += ", " + '\''+ insert_str + '\''     
            insert += ");"
            if debug:
                print(insert)
                print(values)
            try: 
                cur.execute(insert, values)
            except ValueError as e:
                logging.error("Error inserting data")
                logging.error(e)
                logging.error(insert)
                close = input("press enter to close")
                sys.exit()
            num += 1
            if num % 20 == 0:
                logging.info("Inserted " + str(num) + " rows")
        

        conn.commit()
        logging.info("Data inserted")
    except Exception as e:
        print(e)
    if debug:
        close = input("press enter to close")

if __name__ == '__main__':
    __main__()